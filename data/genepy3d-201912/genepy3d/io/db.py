"""
Saving complexe structures such as tree, surface into table style database like h5db might
be not useful. 

This module is temporary skipped.

"""


from genepy3d.util import h5db

class NeuronDb:
    """Support reading neurons from hdf5 database.
    """
    
    def __init__(self, dbname):
        self.dbname = dbname
        
    def get_neuron_id(self,neuron_name=None):
        """Return list of neuron_id from given neuron names.
        """
        db = h5db.H5Db(self.dbname)
        
        if neuron_name is None:
            return db.get_dataframe('dfneu',columns=['neuron_id'])['neuron_id'].unique() # get all neuron ids
        else:
            df = db.get_dataframe('dfneu',columns=['neuron_name','neuron_id']).drop_duplicates()
            df.set_index('neuron_name',inplace=True)
            if isinstance(neuron_name,str): # only one neuron name
                try:
                    return df.loc[neuron_name].values[0]
                except:
                    return None
            else: # array-like
                try:
                    return df.loc[neuron_name].values.flatten()
                except:
                    return None
            
    def get_neuron_name(self,neuron_id=None):
        """Return list of neuron_name from given neuron ids.
        """
        db = h5db.H5Db(self.dbname)
        
        if neuron_id is None:
            return db.get_dataframe('dfneu',columns=['neuron_name'])['neuron_name'].unique()
        else:
            df = db.get_dataframe('dfneu',columns=['neuron_name','neuron_id']).drop_duplicates()
            df.set_index('neuron_id',inplace=True)
            if isinstance(neuron_id,int):
                try:
                    return df.loc[neuron_id].values[0]
                except:
                    return None
            else: # array-like
                try:
                    return df.loc[neuron_id].values.flatten()
                except:
                    return None
            
    def get_neuron_data(self,neuron_id=None,return_type='dataframe'):
        """Return data from given neuron ids.
        
        Issues
            - we haven't handle the case when dfcon, dfcompo are empty for a specific neuron_id.
        
        """
        
        db = h5db.H5Db(self.dbname)
        
        # check neuron_id
        only_flag = False
        if neuron_id is None:
            # we use list instead of numpy array because hdf5 query accept only list type
            neuronlst = list(self.get_neuron_id())
        elif neuron_id is not None:
            if isinstance(neuron_id,int): # only one item.
                only_flag = True
                neuronlst = [neuron_id]
            else: # array-like
                neuronlst = list(neuron_id)
           
        try:
            dfneu = db.get_dataframe('dfneu',query='neuron_id={}'.format(neuronlst))
            if len(dfneu)==0:
                dfneu = None
        except:
            dfneu = None
        
        try:
            dfcon = db.get_dataframe('dfcon',query='neuron_id={}'.format(neuronlst))
            if len(dfcon)==0:
                dfcon = None
        except:
            dfcon = None
        
        try:
            dfcompo = db.get_dataframe('dfcompo',query='neuron_id={}'.format(neuronlst))
            if len(dfcompo)==0:
                dfcompo = None
        except:
            dfcompo = None
            
        
        if return_type=='dataframe':
            return {'dfneu':dfneu, 'dfcon':dfcon, 'dfcompo':dfcompo}
        else: # dict
            dic = {}
            
            if dfneu is None:
                return dic
            
            for neuid in neuronlst:

                subdfneu = dfneu[dfneu['neuron_id']==neuid].copy()
                if len(subdfneu)==0:
                    continue
                else:
                    neuname = subdfneu.iloc[0]['neuron_name'].encode('ascii','ignore') # hdf5 does not support unicode
                    subdfneu = subdfneu[['treenode_id','structure_id','x','y','z','r','parent_treenode_id']]
                
                subdfcon = None
                if dfcon is not None:
                    subdfcon = dfcon[dfcon['neuron_id']==neuid].copy()
                    subdfcon = subdfcon[['connector_id','treenode_id','relation_id']]
                    if len(subdfcon)==0:
                        subdfcon = None
                
                subdfcompo = None
                if dfcompo is not None:
                    subdfcompo = dfcompo[dfcompo['neuron_id']==neuid].copy()
                    subdfcompo = subdfcompo[['treenode_id','group_id']]
                    if len(subdfcompo)==0:
                        subdfcompo = None
                
                dic[neuid] = {'neuron_name':neuname,'dfneu':subdfneu,'dfcon':subdfcon,'dfcompo':subdfcompo}
                
            if only_flag==True:
                if len(dic)!=0:
                    return dic.items()[0][1]
            
            return dic
            
def build_neuron_db(dbname,dfneu,dfcon=None,dfsyn=None,dfcompo=None):
    """Build neuron database.
    
    Args
        dbname (str): database name.
        dfneu (dataframe): neuron data.
        dfcon (dataframe): connector data.
        dfsyn (dataframe): synaptic data.
        dfcompo (dataframe): tree components data.
        
    Returns
        A Hdf5 file is created containing dfneu, dfcompo, dfcon, dfsyn.
           
    Note
        - If the dataframes already existed in the database, they will be overwritten.
    
    """
           
    if dfneu is None:
        raise Exception('Nothing in data.')
    else:
        if len(dfneu)==0:
            raise Exception('Nothing in data.')
    
    db = h5db.H5Db(dbname)
    
    try:
        db.append('dfneu',dfneu)
        
        if dfcon is not None:
            db.append('dfcon',dfcon)
            
        if dfsyn is not None:
            db.append('dfsyn',dfsyn)
            
        if dfcompo is not None:
            db.append('dfcompo',dfcompo)
        
    except:
        raise Exception('Failed when building neuron database.')
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            