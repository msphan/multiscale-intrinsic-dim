#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import requests
from requests.auth import AuthBase
import numpy as np
import pandas as pd

from genepy3d.io import db

class CatmaidApiTokenAuth(AuthBase):
    """Attaches HTTP X-Authorization Token headers to the given Request."""
    
    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers['X-Authorization'] = 'Token {}'.format(self.token)
        return r

class CatmaidServer:
    """Catmaid API wrapper for getting neuron data from Catmaid server.
    
    """
    
    def __init__(self,catmaid_host,token):
        """
    
        Args
            catmaid_host (str): catmaid base url.
            token (str): authentication token string.
            
        """
        self.catmaid_host = catmaid_host
        self.token = token
        self.dfneu = None # neuron dataframe
        self.dfcon = None # connector dataframe
    
    def get_neuron_id(self,project_id):
        """Return neuron ids for given project_id.
        
        Args
            project_id (int): project id.
            
        Returns
            A numpy array [int] of neuron ids.
        
        """

        # restful request
        linkrequest = self.catmaid_host+'{}/skeletons/'.format(project_id)
        
        try:
            res = requests.get(linkrequest,auth=CatmaidApiTokenAuth(self.token))
        except:
            raise ValueError('can not connect to server.')

        neuronlst = np.array(res.json())

        return neuronlst

    def get_neuron_data(self,project_id,neuron_id=None,return_type='dataframe'):
        """Return neuron data given by neuron_id from project_id.
        
        Args
            project_id (int): project id.
            neuron_id (int | array-like[int]): list of neuron ids.
            return_type (str): support 'dataframe'|'dict'.
        
        Returns
            If return_type is 'dataframe', then return a dictionary of dfneu and dfcon where:
            
            - dfneu (pandas dataframe): columns include 'neuron_name', 'neuron_id', treenode_id','structure_id','x','y','z','r','parent_treenode_id'.
            - dfcon (pandas dataframe): columns include 'connector_id','neuron_id','treenode_id','relation_id'.
            
            Else if return_type is 'dict', then a dictionary with format {neuron_id:[neuron_name,dfneu,dfcon],...}.
            Each item of the dictionary is a dictionary includes:
            
            - neuron_name (str)
            - dfneu (pandas dataframe): columns include 'treenode_id','structure_id','x','y','z','r','parent_treenode_id'.
            - dfcon (pandas dataframe): columns include 'connector_id','treenode_id','relation_id'.
            
        Notes
            - the 'dict' returned type is convenient for creating a tree object.
            
        """
        
        # check neuron_id
        only_flag = False
        if neuron_id is None:
            neuronlst = self.get_neuron_id(project_id) # get all neuron ids.
        elif neuron_id is not None:
            if isinstance(neuron_id,int): # only one item.
                only_flag = True
                neuronlst = [neuron_id]
            elif isinstance(neuron_id, (list, np.ndarray)): # array-like
                neuronlst = neuron_id
            else:
                raise Exception('neuron_id must be int or array-like.')
        
        neuinfo, coninfo = [], []
        dic = {}
        
        # query data from catmaid
        for neuid in neuronlst:
            
            linkrequest = self.catmaid_host+'{}/skeleton/{}/json'.format(project_id,neuid)
            res = requests.get(linkrequest,auth=CatmaidApiTokenAuth(self.token)).json()
            
            if isinstance(res,dict): # if neuid is not found, catmaid return a dic.
                continue
            
            # get neuron name
            try:
#                neuname = res[0].encode('ascii','ignore') # hdf5 does not support unicode
                neuname = res[0]
            except:
                neuname = 'genepy3d'
            
            subneu, subcon = [], []
            
            # get neuron info
            try:
                for iske in res[1]:
                    if iske[1] is not None:
                        iske_sub = [neuname, neuid, iske[0], 0, iske[3], iske[4], iske[5], iske[6], iske[1]]
                    else:
                        iske_sub = [neuname, neuid, iske[0], 0, iske[3], iske[4], iske[5], iske[6], -1]
                    subneu.append(iske_sub)
            except:
                subneu = []
            
            # get connector info
            try:
                for icon in res[3]:
                    relation_type = 'presynaptic_to'
                    if icon[2]==1:
                        relation_type = 'postsynaptic_to'
                    icon_sub = [icon[1], neuid, icon[0], relation_type]
                    subcon.append(icon_sub)
            except:
                subcon = []
                
            if return_type=='dict':
                if len(subneu)>0:
                    dfsubneu = pd.DataFrame(subneu,columns=['neuron_name','neuron_id','treenode_id','structure_id','x','y','z','r','parent_treenode_id'])
                    dfsubneu = dfsubneu[['treenode_id','structure_id','x','y','z','r','parent_treenode_id']]
                else:
                    dfsubneu = None
                if len(subcon)>0:
                    dfsubcon = pd.DataFrame(subcon,columns=['connector_id','neuron_id','treenode_id','relation_id'])
                    dfsubcon = dfsubcon[['connector_id','treenode_id','relation_id']]
                else:
                    dfsubcon = None
                    
                dic[neuid] = {'neuron_name':neuname,'dfneu':dfsubneu,'dfcon':dfsubcon}
            
            neuinfo = neuinfo + subneu
            coninfo = coninfo + subcon
                
        # create dataframe from data
        if len(neuinfo)==0:
            dfneu = None
        else:
            dfneu = pd.DataFrame(neuinfo,columns=['neuron_name','neuron_id','treenode_id','structure_id','x','y','z','r','parent_treenode_id'])
        
        if len(coninfo)==0:
            dfcon = None
        else:
            dfcon = pd.DataFrame(coninfo,columns=['connector_id','neuron_id','treenode_id','relation_id'])
        
        self.dfneu = dfneu
        self.dfcon = dfcon

        # check return type        
        if return_type=='dataframe':
            return {'dfneu':dfneu, 'dfcon':dfcon}
        elif return_type=='dict':
            if only_flag==True: # only one item
                if len(dic)!=0:
                    return list(dic.items())[0][1]
            return dic
        else:
            raise ValueError('return_type should be "dataframe" or "dict"')
            
    def export_csv(self,filename1='dfneu.csv',filename2='dfcon.csv'):
        """Export neuron data to csv files.
        
        Data getting by get_neuron_data() will be served for exporting.
        
        Args:
            filename1 (str): dfneu name.
            filename2 (str): dfcon name;
        
        """
        
        if self.dfneu is not None:
            self.dfneu.to_csv(filename1)
            
        if self.dfcon is not None:
            self.dfcon.to_csv(filename2)
        
    
    def export_db(self,dbname):
        """Export neuron data to hdf5 database.
        
        Data getting by get_neuron_data() will be served for exporting.
        
        """
        
        dfcompo = None
        dfsyn = compute_synapse(self.dfcon)
        
        # export to hdf5 db
        db.build_neuron_db(dbname,self.dfneu,self.dfcon,dfsyn,dfcompo)
        
class CatmaidCSV:
    """Support reading neurons from catmaid files.    
    
    """
    
    def __init__(self, neuronfile, confile=None):
        """Load data from csv files.
        
        Args
            neuronfile (str): file containing neuron data.
            confile (str): file containing connection data.
        
        """
        
        # read neuronfile
        try:
            dfneu = pd.read_csv(neuronfile)
        except:
            raise Exception('Failed to load neuron file.')
        
        labels = dfneu.columns.values
        refined_labels = []
        for lbl in labels:
            refined_labels.append(lbl.split()[0].lower()) # extract all column names, remove irregular characters
        if all(lbl in refined_labels for lbl in ['neuron_name','neuron_id','treenode_id','structure_id','x','y','z','r','parent_treenode_id']):
            dfneu.columns = refined_labels
            dfneu.dropna(inplace=True) # drop invalid rows
            dfneu = dfneu[['neuron_name','neuron_id','treenode_id','structure_id','x','y','z','r','parent_treenode_id']] # select only conventional columns
            self.dfneu = dfneu
        elif all(lbl in refined_labels for lbl in ['neuron','skeleton_id','treenode_id','parent_treenode_id','x','y','z','r']):
            dfneu.columns = refined_labels
            dfneu['structure_id'] = 0 # add structure_id column
            dfneu.rename(columns={'neuron':'neuron_name','skeleton_id':'neuron_id'},inplace=True) # rename following the convention
            dfneu = dfneu[['neuron_name','neuron_id','treenode_id','structure_id','x','y','z','r','parent_treenode_id']] # select only conventional columns
            dfneu['parent_treenode_id'].fillna(-1,inplace=True) # replace NaN by -1
            dfneu.dropna(inplace=True) # drop invalid rows
            dfneu['parent_treenode_id'] = dfneu['parent_treenode_id'].astype('int') # cast into int, since type was changed after fillna()
            self.dfneu = dfneu
        else:
            raise ValueError("The file must contain columns 'neuron','skeleton_id','treenode_id','parent_treenode_id','x','y','z','r'.")
        
        # read connector file
        if confile is not None:
            try:
                dfcon = pd.read_csv(confile)
            except:
                raise Exception ('Failed to laod connector file.')
            
            labels = dfcon.columns.values
            refined_labels = []
            for lbl in labels:
                refined_labels.append(lbl.split()[0].lower())
            if all(lbl in refined_labels for lbl in ['connector_id','neuron_id','treenode_id','relation_id']):
                dfcon.columns = refined_labels
                dfcon.dropna(inplace=True) # drop invalid rows
                dfcon = dfcon[['connector_id','neuron_id','treenode_id','relation_id']]
                self.dfcon = dfcon
            elif all(lbl in refined_labels for lbl in ['connector_id','skeleton_id','treenode_id','relation_id']):
                dfcon.columns = refined_labels
                dfcon.rename(columns={'skeleton_id':'neuron_id'},inplace=True) # rename some columns
                dfcon = dfcon[['connector_id','neuron_id','treenode_id','relation_id']]
                dfcon.dropna(inplace=True) # drop invalid rows
                self.dfcon = dfcon
            else:
                raise ValueError("The file must contain columns 'connector_id','skeleton_id','treenode_id','relation_id'.")
        else:
            self.dfcon = None
        
        self.subdfneu = None # neuron dataframe served for get_neuron_data()
        self.subdfcon = None # connector dataframe served for get_neuron_data()
                
    def get_neuron_id(self,neuron_name=None):
        """Return neuron ids.
        
        Args
            neuron_name (str|[str]): list of neuron names.
 
        Returns
            A numpy array [int] of neuron ids.
        
        """
        
        if neuron_name is None:
            return self.dfneu['neuron_id'].unique() # get all neuron ids
        else:
            df = self.dfneu[['neuron_name','neuron_id']].drop_duplicates().copy()
            df.set_index('neuron_name',inplace=True)
            if isinstance(neuron_name,str): # only one neuron name
                try:
                    return df.loc[neuron_name].values[0]
                except:
                    return None
            else: 
                try:
                    return df.loc[neuron_name].values.flatten()
                except:
                    return None
    
    def get_neuron_name(self,neuron_id=None):
        """Return neuron names.
        
        Args
            neuron_id (int|[int]): list of neuron ids.
 
        Returns
            A numpy array [str] of neuron names.
        
        """
        if neuron_id is None:
            return self.dfneu['neuron_name'].unique() # get all neuron_name
        else:
            df = self.dfneu[['neuron_name','neuron_id']].drop_duplicates().copy()
            df.set_index('neuron_id',inplace=True)
            if isinstance(neuron_id,int):
                try:
                    return df.loc[neuron_id].values[0]
                except:
                    return None
            else: # array-like
                try:
                    return df.loc[neuron_id].values.flatten()
                except:
                    return None
    
    def get_neuron_data(self,neuron_id=None,return_type='dataframe'):
        """Return neuron data given by neuron_id.
        
        Args
            neuron_id (int | array-like[int]): list of neuron ids.
            return_type (str): support 'dataframe'|'dict'.
        
        Returns
            If return_type is 'dataframe', then return dictionary of dfneu and dfcon where:
            
            - dfneu (pandas dataframe): columns include 'neuron_name', 'neuron_id', treenode_id','structure_id','x','y','z','r','parent_treenode_id'.
            - dfcon (pandas dataframe): columns include 'connector_id','neuron_id','treenode_id','relation_id'.
            
            Else if return_type is 'dict', then a dictionary with format {neuron_id:[neuron_name,dfneu,dfcon],...}, where:
            
            - dfneu (pandas dataframe): columns include 'treenode_id','structure_id','x','y','z','r','parent_treenode_id'.
            - dfcon (pandas dataframe): columns includd 'connector_id','treenode_id','relation_id'.
            
        Notes
            - the 'dict' returned type is convenient for creating tree object.
            
        """
        
        # check neuron_id
        only_flag = False
        if neuron_id is None:
            neuronlst = list(self.get_neuron_id())
        else:
            if isinstance(neuron_id,int): # only one item.
                only_flag = True
                neuronlst = [neuron_id]
            elif isinstance(neuron_id, (list, np.ndarray)): # array-like
                neuronlst = neuron_id
            else:
                raise Exception('neuron_id must be int or array-like.')
            
        # subset data
        self.subdfneu = self.dfneu[self.dfneu['neuron_id'].isin(neuronlst)].copy()
        if len(self.subdfneu)==0:
            self.subdfneu = None

        if self.dfcon is not None:
            self.subdfcon = self.dfcon[self.dfcon['neuron_id'].isin(neuronlst)].copy()
            if len(self.subdfcon)==0:
                self.subdfcon = None
        
        if return_type=='dataframe':
            return {'dfneu':self.subdfneu, 'dfcon':self.subdfcon}
        elif return_type=='dict': # dict
            dic = {}
            for neuid in neuronlst:
                dfneu = self.dfneu[self.dfneu['neuron_id']==neuid].copy()
                if len(dfneu)!=0:
#                    neuname = dfneu.iloc[0]['neuron_name'].encode('ascii','ignore') # hdf5 does not support unicode
                    neuname = dfneu.iloc[0]['neuron_name']
                    dfsubneu = dfneu[['treenode_id','structure_id','x','y','z','r','parent_treenode_id']]
                else:
                    continue
                
                if self.dfcon is None:
                    dfsubcon = None
                else:
                    dfcon = self.dfcon[self.dfcon['neuron_id']==neuid].copy()
                    if len(dfcon)!=0:
                        dfsubcon = dfcon[['connector_id','treenode_id','relation_id']]
                    else:
                        dfsubcon = None
                
                dic[neuid] = {'neuron_name':neuname,'dfneu':dfsubneu,'dfcon':dfsubcon}
                
            if only_flag==True:
                if len(dic)!=0:
                    return dic.items()[0][1] 
            return dic
        else:
            raise ValueError('return_type should be "dataframe" or "dict".')
            
    def export_db(self, dbname, entire=False):
        """Export data to hdf5 database.
        
        Data getting by get_neuron_data() will be saved into hdf5 database.
        Otherwise whole data from csv files will be saved into hdf5 database.
        
        """
        
        if ((self.subdfneu is None) | (entire==True)):
            dfneu = self.dfneu
            dfcon = self.dfcon
        else:
            dfneu = self.subdfneu
            dfcon = self.subdfcon
        
        dfcompo = None
        dfsyn = compute_synapse(dfcon)
        
        # export to hdf5 db
        db.build_neuron_db(dbname,dfneu,dfcon,dfsyn,dfcompo)

def compute_synapse(dfcon):
    """Compute synapses data from the connector data.
    
    Args
        dfcon (pandas dataframe): connector data.
        
    Returns
       dfsyn (pandas dataframe): pre_neuron_id, pre_treenode_id, post_neuron_id, post_treenode_id.
        
    """
    
    # check dfcon
    if dfcon is None:
        return None
    elif len(dfcon)==0:
        return None
    
    # getting synapse detail
    syninfo = []
    
    conidlist = dfcon['connector_id'].unique()
    for conid in conidlist:
        pre = dfcon[(dfcon['connector_id']==conid)&(dfcon['relation_id']=='presynaptic_to')][['neuron_id','treenode_id']].values.tolist()
        posts = dfcon[(dfcon['connector_id']==conid)&(dfcon['relation_id']=='postsynaptic_to')][['neuron_id','treenode_id']].values.tolist()
        
        if len(pre)==0:
            continue
        elif len(pre)>1:
            raise ValueError('unknown situation: multiple presynaptic_to')
            break
        else:
            if len(posts)==0:
                continue
            else:
                for post in posts:
                    syninfo.append(pre[0]+post)

    # check syninfo
    if len(syninfo)==0:
        return None
    else:
        dfsyn = pd.DataFrame(syninfo,columns=['pre_neuron_id','pre_treenode_id','post_neuron_id','post_treenode_id'])
        return dfsyn
    
    




















































    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
