import os
import glob
import fnmatch
import re
import numpy as np
import pandas as pd

from genepy3d.io import db

class SWC:
    """Support reading swc files.
    """
    
    def __init__(self,filename,recursive=False):
        """
        Args
            filename (str): path to swc file or directory containing swc files.
            recursive (bool): search recursively all swc files in parent directory.
        
        """
    
        self.dfneu = None
        self.subdfneu = None
        
        _ = self._build_neuron_data(filename,recursive)
    
    def _build_neuron_data(self,filename,recursive=False):
        """Build data from swc files.
        
        Args
            filename (str): path to swc file or directory containing swc files.
            recursive (bool): search recursively all swc files in parent directory.
        
        Returns
            pandas dataframe, where columns: neuron_name, neuron_id, treenode_id, structure_id, x, y, z, r, parent_treenode_id
        
        """
        
        filelst = []
        
        # check input
        if os.path.isdir(filename): # if directory
            
            if recursive==False:
                filepattern = os.path.join(filename,"*.swc")
                for file in glob.glob(filepattern):
                    filelst.append(file)
            
            else: # searching recursively from the parent directory
                for root, dirnames, filenames in os.walk(filename):
                    for file in fnmatch.filter(filenames, '*.swc'):
                        filelst.append(os.path.join(root,file))
        
        elif os.path.isfile(filename): # if a file
            filelst.append(filename)
        
        else:  
            raise ValueError("do not support special files (socket, FIFO, device file).")
        
       
        # build data
        data = []
        
        # random assign neuron_id
        neuronidlst = range(len(filelst))
        
        for i in range(len(filelst)):
            
            subdata = []
            file = filelst[i]
            
            # assign neuron id
            neuron_id = neuronidlst[i]
            
            basefile = os.path.basename(file)
            
            # extract neuron name
#            neuron_name = basefile.split(".swc")[0].encode('ascii','ignore') # hdf5 does not support unicode
            neuron_name = basefile.split(".swc")[0]
            
            # extract info from file
            f = open(file,'r')
            for line in f:
                if line[0]!='#':
                    tmp = []
                    
                    elemens = re.split('\r|\n| |\s', line)
                    for ile in elemens:
                        if ile!='':
                            tmp.append(float(ile))
                    
                    tmp = [neuron_name, neuron_id] + tmp
                    subdata.append(tmp)
                    
            f.close()
                
            data = data + subdata
        
        if len(data)!=0:
            # build dataframe and cast columns types
            dfneu = pd.DataFrame(data,columns=['neuron_name','neuron_id','treenode_id', 'structure_id', 'x', 'y', 'z', 'r', 'parent_treenode_id'])
            dfneu.dropna(inplace=True)
            dfneu['neuron_id'] = dfneu['neuron_id'].astype('int')
            dfneu['treenode_id'] = dfneu['treenode_id'].astype('int')
            dfneu['structure_id'] = dfneu['structure_id'].astype('int')
            dfneu['parent_treenode_id'] = dfneu['parent_treenode_id'].astype('int')
            
            self.dfneu = dfneu
    
    def get_neuron_data(self,neuron_id=None,return_type='dataframe'):
        """Return neuron data given by neuron_id.
        
        Args
            neuron_id (int | array-like[int]): list of neuron ids.
            return_type (str): support 'dataframe'|'dict'.
        
        Returns
            If return_type is 'dataframe', then return dictionary of dfneu and dfcon where:
            
            - dfneu (pandas dataframe): columns include 'neuron_name', 'neuron_id', treenode_id','structure_id','x','y','z','r','parent_treenode_id'.
            - dfcon (pandas dataframe): columns include 'connector_id','neuron_id','treenode_id','relation_id'.
            
            Else if return_type is 'dict', then a dictionary with format {neuron_id:[neuron_name,dfneu,dfcon],...}, where:
            
            - dfneu (pandas dataframe): columns include 'treenode_id','structure_id','x','y','z','r','parent_treenode_id'.
            - dfcon (pandas dataframe): columns includd 'connector_id','treenode_id','relation_id'.
            
        Notes
            - the 'dict' returned type is convenient for creating tree object.
            
        Issues
            - we haven't handle the case when dfcon is empty for a specific neuron_id.
            
        """
        
        # check neuron_id
        only_flag = False
        if neuron_id is None:
            neuronlst = list(self.get_neuron_id())
        else:
            if isinstance(neuron_id,(int,np.integer)): # only one item.
                only_flag = True
                neuronlst = [neuron_id]
            elif isinstance(neuron_id, (list, np.ndarray)): # array-like
                neuronlst = neuron_id
            else:
                raise Exception('neuron_id must be int or array-like.')
                
        # subset data
        if self.dfneu is None:
            self.subdfneu = None
        else:
            self.subdfneu = self.dfneu[self.dfneu['neuron_id'].isin(neuronlst)].copy()
            if len(self.subdfneu)==0:
                self.subdfneu = None
        
        if return_type=='dataframe':
            return {'dfneu':self.subdfneu}
        elif return_type=='dict': # dict
            dic = {}
            
            if self.dfneu is None:
                return dic
            
            for neuid in neuronlst:
                dfneu = self.dfneu[self.dfneu['neuron_id']==neuid].copy()
                if len(dfneu)==0:
                    continue
                
#                neuname = dfneu.iloc[0]['neuron_name'].encode('ascii','ignore') # hdf5 does not support unicode
                neuname = dfneu.iloc[0]['neuron_name']
                dfsubneu = dfneu[['treenode_id','structure_id','x','y','z','r','parent_treenode_id']]                
                dic[neuid] = {'neuron_name':neuname,'dfneu':dfsubneu}
                
            if only_flag==True:
                if len(dic)!=0:
                    return list(dic.items())[0][1]
            return dic
        else:
            raise ValueError('return_type should be "dataframe" or "dict".')

    def get_neuron_id(self,neuron_name=None):
        """Return neuron ids.
        
        Args
            neuron_name (str|[str]): list of neuron names.
 
        Returns
            A numpy array [int] of neuron ids.
        
        """
        
        if neuron_name is None:
            return self.dfneu['neuron_id'].unique() # get all neuron ids
        else:
            df = self.dfneu[['neuron_name','neuron_id']].drop_duplicates().copy()
            df.set_index('neuron_name',inplace=True)
            if isinstance(neuron_name,str): # only one neuron name
                try:
                    return df.loc[neuron_name].values[0]
                except:
                    return None
            else: # array-like
                try:
                    return df.loc[neuron_name].values.flatten()
                except:
                    return None
    
    def get_neuron_name(self, neuron_id=None):
        """Return neuron names.
        
        Args
            neuron_id (int|[int]): list of neuron ids.
 
        Returns
            A numpy array [str] of neuron names.
        
        """
        if neuron_id is None:
            return self.dfneu['neuron_name'].unique() # get all neuron_name
        else:
            df = self.dfneu[['neuron_name','neuron_id']].drop_duplicates().copy()
            df.set_index('neuron_id',inplace=True)
            if type(neuron_id) is int:
                try:
                    return df.loc[neuron_id].values[0]
                except:
                    return None
            else: # array-like
                try:
                    return df.loc[neuron_id].values.flatten()
                except:
                    return None
            
    def export_csv(self,filename='dfneu.csv',entire=False):
        """Export neuron data to csv files.
        
        Args:
            filename (str): name for exporting.
            entire (bool): if True, then all data is exported, else data getting by get_neuron_data() will be served for saving.
        
        """
        
        if ((self.subdfneu is None) | (entire==True)):
            dfneu = self.dfneu
        else:
            dfneu = self.subdfneu
            
        if dfneu is not None:
            dfneu.to_csv(filename)

    def export_db(self, dbname, entire=False):
        """Export data to hdf5 database.
        
        Data getting by get_neuron_data() will be saved into hdf5 database.
        Otherwise data from all swc files will be saved into hdf5 database.
        
        """
        
        if ((self.subdfneu is None) | (entire==True)):
            dfneu = self.dfneu
        else:
            dfneu = self.subdfneu
        
        # export to hdf5 db
        db.build_neuron_db(dbname,dfneu=dfneu)


























