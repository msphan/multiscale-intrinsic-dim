import numpy as np
from scipy import interpolate
from scipy.ndimage.filters import gaussian_filter1d
from scipy.ndimage.measurements import label
from scipy.signal import argrelextrema
from genepy3d.util import plot as pl
from genepy3d.util.geo import active_brownian_2d, active_brownian_3d, points_transform, geo_len, norm, fit_plane, angle_threepoint

class Curve:
    """Curve in 3D.
    
    Attributes
        coors (2d numpy array (float) | ([float],[float],[float])): list of 3d points.
    
    """
    def __init__(self, coors, curve_id=None, curve_name=None):
        
        if isinstance(coors,np.ndarray):
            self.coors = coors
        elif isinstance(coors,(tuple,list)):
            self.coors = np.array(coors).T
            
        if curve_id is None:
            self.curve_id = 0 # default value
        else:
            self.curve_id = curve_id
        
        if curve_name is None:
            self.curve_name = 'GeNePy3D'
        else:
            self.curve_name = curve_name
            
        self.length = None
        self.curvature = None
        self.torsion = None
        self.norm = None
        
    def derv(self,deg,dt=1):
        """Derivative using np.gradient.
        
        Args
            deg (int): degree.
            dt (float): delta.
        
        """
        
        dx, dy, dz = self.coors[:,0].copy(), self.coors[:,1].copy(), self.coors[:,2].copy()
        for i in range(deg):
            dx = np.gradient(dx,dt,edge_order=1)
            dy = np.gradient(dy,dt,edge_order=1)
            dz = np.gradient(dz,dt,edge_order=1)

        return np.array([dx, dy, dz]).T
    
    def get_norm(self):
        """Norms of points in curve.
        """
        
        if self.norm is None:
            self.norm = norm(self.coors)

        return self.norm

    def get_length(self):
        """(Geodesic) length of curve.
        """
        
        if self.length is None:
            self.length = geo_len(self.coors)
        
        return self.length
    
    def get_curvature(self):
        
        if self.curvature is None:
        
            d1 = self.derv(1)
            d2 = self.derv(2)
            
            d1norm = norm(d1)
            cp = np.cross(d1,d2)
            cpnorm = norm(cp)
            
            res = np.zeros(len(cpnorm))
            idx = np.argwhere(d1norm!=0).flatten()
            if len(idx)!=0:
                res[idx]  = cpnorm[idx]/(d1norm[idx]**3)
            
            self.curvature = res
        
        return self.curvature
    
    def get_torsion(self):
        
        if self.torsion is None:
        
            d1 = self.derv(1)
            d2 = self.derv(2)
            d3 = self.derv(3)
            
            cp = np.cross(d1,d2)
            cpnorm = norm(cp)
            
            res = np.zeros(len(cpnorm))
            idx = np.argwhere(cpnorm!=0).flatten()
            if len(idx)!=0:
                res[idx] = np.sum(cp[idx]*d3[idx],axis=1)/(cpnorm[idx]**2)
            
            self.torsion = res
        
        return self.torsion
    
    def get_wiggliness(self):
        
        if self.wiggliness is None:
            
            dist = np.sqrt(np.sum((self.coors[0]-self.coors[-1])**2))
            if dist == 0:
                self.wiggliness = 0.
            else:
                self.wiggliness = self.get_length()*1./dist
                
        return self.wiggliness
    
    def get_curviness(self):
        
        if self.curviness is None:
            
            kappa = self.get_curvature()
            extid = argrelextrema(kappa,np.greater)[0] # get indices of local maxima of curvatures
            
            k = len(extid)
            if k==0:
                self.curviness = 0
            else:
                self.curviness = (1./(1.+abs(k)))*np.sum(kappa[extid])
                
        return self.curviness
    
    
    def _extract_plane(self,tor_thr,cur_thr,ext_thr):
        """Compute the local planes (include lines) from torsion, curvature values.
        
        This is the support function for scale_space().
        
        Args
            tor_thr (float): torsion threshold
            cur_thr (float): curvature threshold
            ext_thr (float): threshold for penalize extreme values of torsions (this is not useful)
        
        """
        
        tor = self.get_torsion()
        cur = self.get_curvature()
        
        T = np.zeros(len(tor),dtype=np.uint)
        
#        idx1 = np.argwhere((np.abs(tor) <= tor_thr)|(np.abs(tor) >= ext_thr)).flatten()
        idx1 = np.argwhere((np.abs(tor) <= tor_thr)).flatten()
        
        idx2 = np.argwhere(cur <= cur_thr).flatten()
        
        idx = np.union1d(idx1,idx2)
        
        if len(idx)!=0:
            T[idx] = 1 # plane flag
        
        return T

    def get_scaled_curve(self,sigma):
        """Compute curve at a scale sigma by Gaussian convolution.
        
        Args
            sigma (float): scale.
        
        """
        
        mo = "nearest"
        kerlen = 10
        x, y, z = self.coors[:,0], self.coors[:,1], self.coors[:,2]
        xs = gaussian_filter1d(x,sigma,mode=mo,truncate=kerlen)
        ys = gaussian_filter1d(y,sigma,mode=mo,truncate=kerlen)
        zs = gaussian_filter1d(z,sigma,mode=mo,truncate=kerlen)
        return Curve(np.array([xs,ys,zs]).T)
    
    def scale_space(self,scale_range,features={'curvature','torsion','ridge','valley','planeline','line'},eps_kappa=0.01,eps_tau=0.01,eps_seg=10):
        """Compute scale space of curve.
        
        Can compute various features at one.
        
        Args
            scale_range (array-like [float]): range of scales.
            features (dic[str]): list of features (detail see below).
            eps_kappa (float): curvature threshold.
            eps_tau (float): torsion threshold.
            eps_seg (float): segment length threshold.
            
        Returns
            dictionary whose item is a scale space matrix of a specific feature.
            
        Note
            Support features:
                - curvature: curvature scale space.
                - torsion: torsion scale space.
                - ridge: local maxima of curvature
                - valley: local minima of curvature
                - planeline: plane+line scale space.
                - line: line scale space.
        
        """
        
        x, y, z = self.coors[:,0], self.coors[:,1], self.coors[:,2]
        xs, ys, zs = x.copy(), y.copy(), z.copy()
        
        npoints = self.coors.shape[0]
        
        # initialize data
        data = {}
        for f in features:
            data[f] = np.zeros((len(scale_range),npoints),dtype=np.float)
        
        # default parameter of gaussian filter
        mo = "nearest"
        kerlen = 10
        
        for i in range(len(scale_range)):
            
            # scaled curve
            if scale_range[i]!=0:
                # smooth curve with gaussian func
                xs = gaussian_filter1d(x,scale_range[i],mode=mo,truncate=kerlen)
                ys = gaussian_filter1d(y,scale_range[i],mode=mo,truncate=kerlen)
                zs = gaussian_filter1d(z,scale_range[i],mode=mo,truncate=kerlen)
            scaled_curve = Curve(coors=(xs,ys,zs))
            cur = scaled_curve.get_curvature()
            tor = scaled_curve.get_torsion()
            
            # curvature
            if 'curvature' in features:
                data['curvature'][i,:] = cur

            # torsion                
            if 'torsion' in features:
                data['torsion'][i,:] = tor

            # ridge 
            if 'ridge' in features:
                extid = argrelextrema(cur,np.greater)[0]
                data['ridge'][i,extid] = 1
            
            # valley
            if 'valley' in features:
                extid = argrelextrema(cur,np.less)[0]
                data['valley'][i,extid] = 1
            
            # estimate local planes (include also lines)
            if 'planeline' in features:
                ext_thr = 1e6 # peak threshold (not useful, so make very large value) => see extract_plane()
                data['planeline'][i,:] = scaled_curve._extract_plane(eps_tau,eps_kappa,ext_thr)
                connected_compo, nb_compo = label(data['planeline'][i,:])
                if nb_compo!=0:
                    for j in range(1,nb_compo+1):
                        connected_idx = np.argwhere(connected_compo==j).flatten()
                        if len(connected_idx)<eps_seg:
                            data['planeline'][i,connected_idx] = 0  # remove plane artifacts based on segment length threshold
                        
            # estimate local lines
            if 'line' in features:
                idx = np.argwhere(cur <= eps_kappa).flatten()
                if len(idx)!=0:
                    data['line'][i,idx] = 1
                    
                connected_compo, nb_compo = label(data['line'][i,:])
                if nb_compo!=0:
                    for j in range(1,nb_compo+1):
                        connected_idx = np.argwhere(connected_compo==j).flatten()
                        if len(connected_idx)<eps_seg:
                            data['line'][i,connected_idx] = 0  # remove plane artifacts

        return data
                
    def _find_best_segments(self,I):
        """Estimate the best combination of planes or lines within scale intervals [l1,l2] from planes or lines evolution data.
        
        This is the support function for decompose_intrinsicdim().
        
        Args
            I (2d numpy array [float]): scale space (planeline or line).
            
        Returns
            dictionary containing all estimated segments info.
        
        """
        
        nb_seg = [] # nb of segments at each scale
        seg_info = [] # begin and end indices of each segment at each scale
        seg_len = [] # segment length at each scale
        nb_seg_groups = [] # list of various combinations of segments within the scale range
        duration = [] # number of scale each combination of segment appear within the scale range
        
        nscale, npoint = I.shape
        
        it = 0 # iterator for nb_seg_groups
        
        # check segments duration
        for il in range(nscale):
            
            # get connected components at this scale
            connected_compo, nb_compo = label(I[il])
            nb_seg.append(nb_compo)
            
            # save segment information
            if nb_compo==0:
                seg_info.append([])
                seg_len.append([])
            else:
                tmp, tmp2 = [], []
                for icompo in range(1,nb_compo+1):
                    connected_idx = np.argwhere(connected_compo==icompo).flatten()
                    tmp.append([connected_idx[0],connected_idx[-1]]) # save component indice interval
                    tmp2.append(len(connected_idx)) # save component length
                seg_info.append(tmp)
                seg_len.append(tmp2)
            
            # calculate its duration
            if len(nb_seg_groups)==0: # this first time
                nb_seg_groups.append(nb_compo)
                duration.append(1)
            elif nb_seg_groups[it]==nb_compo: # update step: compare states between the current scale with the previous scale (it)
                duration[it] = duration[it]+1
            else: # if different, reset to new combination
                it = it + 1
                nb_seg_groups.append(nb_compo)
                duration.append(1)
        
        # select the interval that show the longest seg life
        nb_seg, nb_seg_groups, duration = np.array(nb_seg),np.array(nb_seg_groups),np.array(duration)
        tmp_duration = duration.copy()
        idx = np.argwhere(nb_seg_groups==0).flatten() # check seg groups that have nothing
        if len(idx)!=0:
            tmp_duration[idx] = -1 # penalize 0 segments
        best_group = np.argmax(tmp_duration) # the best combination is the longest life
        
        # compute max, min and avg scales within the best combination
        max_level = int(np.sum(duration[:best_group+1]) - 1)
        min_level = int(np.sum(duration[:best_group]))
        avg_level = int((max_level + min_level)/2)
    
        # estimate best configuration of segments from the best combination
        # NOTE: the nb. of segments is similar across scales within the best combination but with various lengths.
        # Thus, select the subinterval of seg_len within the maximal range
        subseg = np.array(seg_len[min_level:max_level+1]) 
        max_seg_ids = np.argmax(subseg,axis=0)+min_level # get the maximal length for each component
    
        # combine all segments indices into one array
        best_seg_ids = np.zeros(npoint,dtype=np.uint)
        for iseg in range(len(max_seg_ids)):
            id1, id2 = seg_info[max_seg_ids[iseg]][iseg][0], seg_info[max_seg_ids[iseg]][iseg][1]
            best_seg_ids[id1:id2+1] = best_seg_ids[id1:id2+1] + 1
        
        # show intersected segments (marked by value > 1)
        conflit_ids = np.zeros(npoint,dtype=np.uint)
        idx = np.argwhere(best_seg_ids>1).flatten() # the intersected zones are marked by value > 1.
        conflit_ids[idx]=1
        if len(idx)!=0:
            connected_compo, nb_compo = label(conflit_ids) # get all intersected zones
            for icompo in range(1,nb_compo+1):
                connected_idx = np.argwhere(connected_compo==icompo).flatten()
                best_seg_ids[connected_idx] = 0 # first assign this zone by 0
                if len(connected_idx)>2: # if the intersected segment has a least 2 points.
                    seg_1 = connected_idx[0:len(connected_idx)/2]
                    seg_2 = connected_idx[(len(connected_idx)/2)+1:]
                    best_seg_ids[seg_1] = 1
                    best_seg_ids[seg_2] = 1
    
        # finally, extract again all segments from best_seg_ids
        pred_segs = []
        connected_compo, nb_compo = label(best_seg_ids)
        if nb_compo!=0:
            for icompo in range(1,nb_compo+1):
                connected_idx = np.argwhere(connected_compo==icompo).flatten()
                pred_segs.append([connected_idx[0],connected_idx[-1]])
        
        dic = {}
        dic['pred_segs'] = pred_segs
        dic['max_level'] = max_level
        dic['min_level'] = min_level
        dic['avg_level'] = avg_level
        dic['nb_seg'] = nb_seg
        dic['seg_info'] = seg_info
        dic['seg_len'] = seg_len
        dic['nb_seg_groups'] = nb_seg_groups
        dic['duration'] = duration
        
        return dic    
    
    def decompose_intrinsicdim(self,sig_c,delta_sig,sig_step,eps_kappa,eps_tau,eps_seg):
        """Decompose curve into hierarchichal intrinsic segments.
        
        Args
            sig_c (float): central scale of the searching region.
            delta_sig (float): Percentage of sig_c for determining the larger of the searching region.
            sig_step (int): scale step within the searching region.
            eps_kappa (float): curvature threshold.
            eps_tau (float): torsion threshold.
            eps_seg (float): segment length threshold.
            
        Return
            list of segment indices specifying line, plane+line.
        
        """
        
        # setting scale range
        l1 = sig_c - sig_c*delta_sig
        if l1 < 0:
            l1 = 0
        l2 = sig_c + sig_c*delta_sig
        scale_range = np.arange(l1,l2+sig_step,sig_step)
        
        # compute planeline and line indicators
        data = self.scale_space(scale_range,{'planeline','line'},eps_kappa,eps_tau,eps_seg)
        
        planeline_param = self._find_best_segments(data['planeline'])
        min_pl_level = planeline_param['min_level']
        max_pl_level = planeline_param['max_level']
        
        line_param = self._find_best_segments(data['line'][min_pl_level:max_pl_level+1])
        
        self.intrinsic_dims = {}
        self.intrinsic_dims['scale_range'] = scale_range
        self.intrinsic_dims['planeline_scales'] = data['planeline']
        self.intrinsic_dims['line_scales'] = data['line']
        self.intrinsic_dims['planeline_param'] = planeline_param
        self.intrinsic_dims['line_param'] = line_param

        plids = planeline_param['pred_segs']
        lids = line_param['pred_segs']
        return {'planeline_pred':plids,'line_pred':lids}
    
    def plot_decomposed_table(self,ax,show_scales=True):
        """Display decomposed intrinsic table.
        
        This is used with decompose_intrinsicdim().
        
        """
        
        npoints = len(self.coors)
        s = 10

        ax.imshow(self.intrinsic_dims['planeline_scales'],cmap='gray',aspect=3)
        ax.contourf(self.intrinsic_dims['line_scales'], 1, hatches=['', '//// \\\\\\\\'], alpha=0.1) # use this to mark line within plane
        
        if show_scales == True:
            ax.set_yticks(np.arange(0,len(self.intrinsic_dims['scale_range']),s))
            ax.set_yticklabels(np.round(self.intrinsic_dims['scale_range'][0::s],2))
        else:
            ax.set_yticks(np.arange(0,len(self.intrinsic_dims['scale_range']),s))
        
        min_pl_level = self.intrinsic_dims['planeline_param']['min_level']
        max_pl_level = self.intrinsic_dims['planeline_param']['max_level']
        min_l_level = self.intrinsic_dims['line_param']['min_level']
        max_l_level = self.intrinsic_dims['line_param']['max_level']
        
        ax.hlines(min_pl_level,0,npoints,color='yellow',linewidth=5)
        ax.hlines(max_pl_level,0,npoints,color='yellow',linewidth=5)
        ax.hlines(min_pl_level+min_l_level,0,npoints,color='blue')
        ax.hlines(min_pl_level+max_l_level,0,npoints,color='blue')
        ax.set_xlim(0,npoints);
        
        ax.invert_yaxis();
        ax.grid('on');
        
    def plot_intrinsicdim(self,ax,projection='3d',scales=(1.,1.,1.)):
        """Display curve marked with intrinsic dimensionalities.
        
        This is used with decompose_intrinsicdim().
        
        """
        
        P = self.coors
        
        pl.plot_point(ax,projection,P[0,0],P[0,1],P[0,2],scales,point_args={'c':'red','s':40})
        
        pl.plot_line(ax,projection,P[:,0],P[:,1],P[:,2],scales,line_args={'c':'green','alpha':0.7})        
        
        pred_pl_ids = self.intrinsic_dims['planeline_param']['pred_segs']
        pred_l_ids = self.intrinsic_dims['line_param']['pred_segs']
        for i in pred_pl_ids:
            idx = range(i[0],i[1]+1)
            pl.plot_line(ax,projection,P[idx,0],P[idx,1],P[idx,2],scales,line_args={'c':'yellow','lw':5,'alpha':0.7})
            if projection == '3d':
                data = P[idx]
                c, normal = fit_plane(data)
                maxx = np.max(data[:,0])
                maxy = np.max(data[:,1])
                minx = np.min(data[:,0])
                miny = np.min(data[:,1])
                
                pnt = np.array([0.0, 0.0, c])
                d = -pnt.dot(normal)
                
                xx, yy = np.meshgrid([minx, maxx], [miny, maxy])
                z = (-normal[0]*xx - normal[1]*yy - d)*1. / normal[2]
                
                ax.plot_surface(xx, yy, z, color='yellow',alpha=0.4)
            
        for i in pred_l_ids:
            idx = range(i[0],i[1]+1)
            pl.plot_line(ax,projection,P[idx,0],P[idx,1],P[idx,2],scales,line_args={'c':'blue','alpha':0.7})
            
        if projection != '3d':
                ax.axis('equal')
        else:
            param = pl.fix_equal_axis(self.coors / np.array(scales))
            ax.set_xlim(param['xmin'],param['xmax'])
            ax.set_ylim(param['ymin'],param['ymax'])
            ax.set_zlim(param['zmin'],param['zmax'])
            
    def _find_ridge(self,idx,C,step):
        """Find ridge starting from index idx at level 0 (noiseless case) in scale space C. 
        
        The interval size to find the next closest index of upper level is specified by step.
        
        This is the support function for principal_turns().
        
        Args:
            idx (int): index to start identifying the ridge from sigma = 0.
            C (2d numpy array [float]): matrix of curvature scale space.
            step (int): threshold used to search the next closest index in the upper level.
            
        Returns:
            Numpy array specifying the indices of ridge and a warning flag.
            
        Note:
            A warning flag is returnned, if warning=1, then there's error when finding ridge.
                It could be due to the conflicts with anothers ridges during ridge evolution.
              
        """
        check_idx = idx
        ridge_idx = []
        ridge_idx.append((0,check_idx)) # add first ridge index
    
        warning = 0
        m, n = C.shape[0], C.shape[1]
    
        for i in range(1,m):
            
            subC = np.zeros(2*step+1)
            subC_start, subC_stop = 0, 2*step
        
            C_start, C_stop = check_idx-step, check_idx+step
        
            # check border condition
            if (check_idx-step)<0:
                C_start = 0
                subC_start = 0 - (check_idx-step)
                
            if (check_idx+step)>(n-1):
                C_stop = n-1
                subC_stop = (2*step) - ((check_idx+step) - (n-1))
        
            # extract local maxima at the upper level between interval specified by step
            subC[subC_start:(subC_stop+1)] = C[i,C_start:(C_stop+1)]
        
            candidate_idx = np.argwhere(subC!=0).flatten()
        
            # select the suitable index based on distance
            if len(candidate_idx)==0:
                break
            elif len(candidate_idx)==1:
                check_idx = check_idx + candidate_idx[0] - step
                ridge_idx.append((i,check_idx))
            else:
                darr = np.abs(candidate_idx-step)
                smallest_idx = np.argwhere(darr==min(darr)).flatten()
                if len(smallest_idx)==1:
                    check_idx = check_idx + candidate_idx[smallest_idx[0]] - step
                    ridge_idx.append((i,check_idx))
                else:
                    warning = 1
                    break
    
        return (np.array(ridge_idx), warning)
    
    def _removepnt_byangle(self,breakid,angle_thr):
        """Suppose set of points S on a curve (x(t),y(t),z(t)) specified by indices in breakid, remove points from S based on their angles.
        
        This is support function() for principal_turns().
        
        Returns:
            new break indices.
        
        """
        
        x, y, z = self.coors[:,0], self.coors[:,1], self.coors[:,2]
        
        newbreakid = breakid.copy()
        idx = 2
        while(idx<len(newbreakid)):
            ia = newbreakid[idx-2]
            ib = newbreakid[idx-1]
            ic = newbreakid[idx]
    
            a = np.array([x[ia],y[ia],z[ia]])
            b = np.array([x[ib],y[ib],z[ib]])
            c = np.array([x[ic],y[ic],z[ic]])
            
            if angle_threepoint(a,b,c)<=angle_thr:
                newbreakid = np.delete(newbreakid,(idx-1))
            else:
                idx = idx + 1
        return newbreakid
    
    def principal_turns(self,sig_c,delta_sig,sig_step,search_step,eps_kappa,ridgelength_thr,angle_thr):
        """Compute the principal turns of curve.
        
        Args
            sig_c (float): central scale of the searching region.
            delta_sig (float): Percentage of sig_c for determining the larger of the searching region.
            sig_step (int): scale step within the searching region.
            search_step (int): number of up-scales to search for the next point of ridge.
            eps_kappa (float): curvature threshold.
            ridgelength_thr (float): ridge length threshold.
            angle_thr (float): angle threshold.
            
        Returns
            indices of principal turns.        
        
        """
        
        # local maximal curvatures indices at the original scale
        extid = argrelextrema(self.get_curvature(),np.greater)[0]
        
        # setting scale range
        # setting scale range
        l1 = sig_c - sig_c*delta_sig
        if l1 < 0:
            l1 = 0
        l2 = sig_c + sig_c*delta_sig
        scale_range = np.arange(l1,l2+sig_step,sig_step)
#        scale_range = np.arange(sig1,sig2,sig_step)
        
        # scale space
        data = self.scale_space(scale_range,features={'curvature','ridge'})
        
        # identify ridges in curvature scale space
        ridge_lst, length_lst, warning_lst = [],[],[]
        start_idx = np.argwhere((data['ridge'][0,:]!=0)&(data['curvature'][0,:]>eps_kappa)).flatten()
        for idx in start_idx:
            ridge,warning = self._find_ridge(idx,data['ridge'],search_step)
            ridge_lst.append(ridge)
            length_lst.append(len(ridge))
            warning_lst.append(warning)
        length_arr = np.array(length_lst)
        
        if (len(length_arr)==0):
            return None
    
        # select ridges based on ridge length
        p = max(length_arr)*ridgelength_thr
        selected_ridges = length_arr>=p
        selected_idx = start_idx[selected_ridges]
    
        # remove indices based on angle criteria
        npoints = self.coors.shape[0]
        breakid = np.sort(np.array([0]+selected_idx.tolist()+[npoints-1]))
        newbreakid = self._removepnt_byangle(breakid,angle_thr)
        
        # remove points very close to two sides (to compensate the error? but not know where...)
        # Modify into distance threshold
        if len(newbreakid)>2:
            if(newbreakid[1]<(1/50.)*npoints):
                newbreakid = np.delete(newbreakid,1)
                
        if len(newbreakid)>2:
            idx = len(newbreakid)-2
            if((npoints-1-newbreakid[idx])<(1/50.)*npoints):
                newbreakid = np.delete(newbreakid,idx)
                
        selected_idx = newbreakid[1:-1]
        ignored_idx = np.setdiff1d(extid,selected_idx)
        
        self.ppt = {}
        self.ppt['scale_range'] = scale_range
        self.ppt['curvature_scales'] = data['curvature']
        self.ppt['ridge_scales'] = data['ridge']
        self.ppt['ridge_lst'] = ridge_lst
        self.ppt['ridge_ids'] = start_idx
        self.ppt['selected_ridge_ids'] = start_idx[selected_ridges]
        self.ppt['ppt_ids'] = selected_idx
        self.ppt['excluded_ids'] = ignored_idx
        
        return selected_idx
    
    def plot_ridge_map(self,ax,show_scales=True):
        """Display ridge finding from principal turns computation.
        
        """
        
        ridge_ids = self.ppt['ridge_ids']
        ridge_lst = self.ppt['ridge_lst']
        selected_ridge_lst = self.ppt['selected_ridge_ids']
        ppt_ids = self.ppt['ppt_ids']
        scale_range = self.ppt['scale_range']
        for i in range(len(ridge_ids)):
            rid = ridge_ids[i]
            r = ridge_lst[i]
            if rid in selected_ridge_lst:
                if rid in ppt_ids:
                    ax.plot(r[:,1],r[:,0],c='green')
                else:
                    ax.plot(r[:,1],r[:,0],c='magenta')
            else:
                ax.plot(r[:,1],r[:,0],c='red')

        ax.set_ylim(0,len(scale_range))
        ax.set_yticks(range(len(scale_range)))
        
        if show_scales == True:
            ax.set_yticklabels(scale_range)

class SimuIntrinsic:
    """Simulation curve with intrinsic dimensions. 
    
    The process is simulated by 2D and 3D Brownian motion.
    
    """
    
    def __init__(self,nb_seg=None,max_seg=4,para_3d=None,para_plane=None,para_line=None,npoints=1000,sigma=2.):
        """
        
        Args
            nb_seg (int): number of segments.
            max_seg (int): maximal number of segments.
            para_3d (dic): 3d segment parameters.
            para_plane (dic): plane segment parameters.
            para_line (dic): line segment parameters.
            npoints (int): number of points on simulated curve.
            sigma (float): for adding gaussian noise.         
        
        """
        
        self.nb_seg = nb_seg
        self.max_seg = max_seg
        self.npoints = npoints
        self.sigma = sigma
        
        # preset 3d default params range
        n_3d_range = [np.uint32(3*1e4)]
        v_3d_range = [1*1e-6]
        omega_3d_range = [2*np.pi]
        zoom_3d_range = range(5,11,1)
        self.para_3d = {'n_3d_range':n_3d_range, 'v_3d_range':v_3d_range, 'omega_3d_range':omega_3d_range, 'zoom_3d_range':zoom_3d_range}
        if para_3d is not None:
            if 'n_3d_range' in para_3d.keys():
                self.para_3d['n_3d_range'] = para_3d['n_3d_range']
            if 'v_3d_range' in para_3d.keys():
                self.para_3d['v_3d_range'] = para_3d['v_3d_range']
            if 'omega_3d_range' in para_3d.keys():
                self.para_3d['omega_3d_range'] = para_3d['omega_3d_range']
            if 'zoom_3d_range' in para_3d.keys():
                self.para_3d['zoom_3d_range'] = para_3d['zoom_3d_range']
                
        # preset plane default params
        n_plane_range = [np.uint32(3*1e4)]
        v_plane_range = [i*1e-6 for i in range(10,12,1)]
        self.para_plane = {'n_plane_range':n_plane_range, 'v_plane_range':v_plane_range}
        if para_plane is not None:
            if 'n_plane_range' in para_plane.keys():
                self.para_plane['n_plane_range'] = para_plane['n_plane_range']
            if 'v_plane_range' in para_plane.keys():
                self.para_plane['v_plane_range'] = para_plane['v_plane_range']
                
        # preset line default params
        len_line_range = range(50,201,10)
        self.para_line = {'len_line_range':len_line_range}
        if para_line is not None:
            if 'len_line_range' in para_line.keys():
                self.para_line['len_line_range'] = para_line['len_line_range']
                
    def _gen_seg_lbl(self):
        """Generate a list of segment labels: "0" is pure 3d, "1" is plane and "2" is line.
        """
        
        if self.seg_lbl is not None:
            return
        
        # init random seed
        if self.seed_point is not None:
            np.random.seed(self.seed_point)
        
        # init a random nb. of segment <= max_seg 
        if self.nb_seg is not None:
            nb_seg = self.nb_seg
        else:
            nb_seg = np.random.permutation(range(1,self.max_seg+1))[0] 
        
        # init a random nb. of plane and line segments <= nb_seg 
        nb_planes_lines = np.random.permutation(range(1,nb_seg+1))[0] 
        
        seg_lbl = np.zeros(nb_seg,dtype=np.uint8)
        ids = np.random.permutation(nb_seg)[:nb_planes_lines] # random set of plane and line indices
        seg_lbl[ids] = np.random.randint(1,3,len(ids)) # random assign plane or line labels
        
        # convert to list
        seg_lbl = seg_lbl.tolist()
        
        # remove "0,0,.." pattern
        ilbl = 0
        while (ilbl<len(seg_lbl)):
            if(seg_lbl[ilbl]!=0):
                ilbl = ilbl + 1
            elif(ilbl+1)<len(seg_lbl):
                if seg_lbl[ilbl+1]==0:
                    seg_lbl.pop(ilbl)
                else:
                    ilbl = ilbl + 1
            else:
                ilbl = ilbl + 1
    
        self.seg_lbl = seg_lbl
    
    def _gen_seg_para(self):
        """Generate parameters for each segment from seg_lbl.
        The inputs para_3d, para_plane, para_line are the setting of segment
        """
        
        if self.seed_point is not None:
            np.random.seed(self.seed_point)
        
        n_3d_range = self.para_3d['n_3d_range']
        v_3d_range = self.para_3d['v_3d_range']
        omega_3d_range = self.para_3d['omega_3d_range']
        zoom_3d_range = self.para_3d['zoom_3d_range']
        
        n_plane_range = self.para_plane['n_plane_range']
        v_plane_range = self.para_plane['v_plane_range']
        
        len_line_range = self.para_line['len_line_range']
        
        seg_para = []
        for ilbl in self.seg_lbl:
            record = {}
            if ilbl==0: # pure 3d segment
                record['lbl'] = '3d'
                
                idx = np.random.permutation(range(len(n_3d_range)))[0]
                record['n'] = n_3d_range[idx]
                
                idx = np.random.permutation(range(len(v_3d_range)))[0]
                record['v'] = v_3d_range[idx]
                
                idx = np.random.permutation(range(len(omega_3d_range)))[0]
                record['omega'] = omega_3d_range[idx]
                
                idx = np.random.permutation(range(len(zoom_3d_range)))[0]
                record['zoom'] = zoom_3d_range[idx]
            elif ilbl==1: # plane
                record['lbl']='plane'
                
                idx = np.random.permutation(range(len(n_plane_range)))[0]
                record['n'] = n_plane_range[idx]
                
                idx = np.random.permutation(range(len(v_plane_range)))[0]
                record['v'] = v_plane_range[idx]
            else: # line
                record['lbl']='line'
                
                idx = np.random.permutation(range(len(len_line_range)))[0]
                record['len'] = len_line_range[idx]
                
            seg_para.append(record)
            
        self.seg_para = seg_para
        
    def gen(self,seed_point=None,seg_lbl=None):
        """Simulate a 3D curve with intrinsic lines, planes.
        
        Args
            seed_point (int): random state.
            seg_lbl (list[int]): list of segment labels. If None, random segments are assigned.
           
        Issue
            generated curve for noiseless case can contain duplicata.
            
        """
        
        self.seed_point = seed_point
        self.seg_lbl = seg_lbl
        
        self._gen_seg_lbl()
        
        self._gen_seg_para()
        
        tmp = []
        ipl, iln = 0,0
        seg_len = np.zeros(len(self.seg_para))
        
        # read each segment para
        for iseg in range(len(self.seg_para)):
            par = self.seg_para[iseg]
            if par['lbl']=='3d':
                # generate 3d active brownian process
                P,_ = active_brownian_3d(n=par['n'],v=par['v'],omega=par['omega'],seed_point=self.seed_point)
                P = P * 1e6 # to micron
                
                # interpolation
                coef, to = interpolate.splprep([P[:,0],P[:,1],P[:,2]],k=1,s=0)
                ti = np.linspace(0,1,1000)
                xi, yi, zi = interpolate.splev(ti,coef)
                Pi = np.array([xi,yi,zi]).T
                
                # random transformation
                phi, theta, psi = (((2*np.pi) - (0.)) * np.random.random(3) + (0.))
                zx,zy,zz = par['zoom'], par['zoom'], par['zoom']
                Pt = points_transform(Pi,phi,theta,psi,0.,0.,0.,zx,zy,zz)
                
                tmp.append(Pt)
                seg_len[iseg] = geo_len(Pt)
                
            elif par['lbl']=='plane':
                
                ipl = ipl + 1 # plane numerator
                
                # generate 2d active brownian process
                P,_ = active_brownian_2d(n=par['n'],v=par['v'],seed_point=self.seed_point)
                P = P * 1e6 # to micron
                P = np.append(P,np.zeros((P.shape[0],1)),axis=1) # add z coordinates
                
                # interpolation
                coef, to = interpolate.splprep([P[:,0],P[:,1],P[:,2]],k=1,s=0)
                ti = np.linspace(0,1,1000)
                xi, yi, zi = interpolate.splev(ti,coef)
                Pi = np.array([xi,yi,zi]).T
                
                # random transformation
                alpha = 2*np.pi/3.
                beta = (((2*np.pi) - (0.)) * np.random.random(1) + (0.))[0]
                if (ipl%2)!=0:
                    phi, theta, psi = alpha,0.,beta
                else:
                    phi, theta, psi = 0.,alpha,beta
                
                Pt = points_transform(Pi,phi,theta,psi,0.,0.,0.)
                
                tmp.append(Pt)
                seg_len[iseg] = geo_len(Pt)
                
            else: # line
                
                iln = iln + 1 # line numerator
                
                # a simple line in z axis
                P = np.array([np.zeros(par['len']),np.zeros(par['len']),np.arange(par['len'])]).T
                
                # random transformation
                alpha = -np.pi/3.
                beta = (((2*np.pi) - (0.)) * np.random.random(1) + (0.))[0]
                if (iln%2)!=0:
                    phi, theta, psi = alpha,0.5*alpha,beta
                else:
                    phi, theta, psi = 0.5*alpha,alpha,beta
                
                Pt = points_transform(P,phi,theta,psi,0.,0.,0.)
                
                tmp.append(Pt)
                seg_len[iseg] = geo_len(Pt)
                
    
        # concatenate segments into curve
        Pcrv = []
        break_ids = np.zeros(len(self.seg_para)+1,dtype=np.int16)
        plane_ids = []
        line_ids = []
        
        s = 0
        full_len = np.sum(seg_len)
        for iseg in range(len(self.seg_para)):
            # compute proportion of nb. of. points w.r.t. segment length
            if iseg!=(len(self.seg_para)-1):
                npoints_seg = int((seg_len[iseg]*1./full_len)*self.npoints)
                s = s + npoints_seg
            else:
                npoints_seg = self.npoints - s
            
            # interpolation
            P = tmp[iseg]
            coef, to = interpolate.splprep([P[:,0],P[:,1],P[:,2]],k=1,s=0)
            ti = np.linspace(0,1,npoints_seg)
            xi, yi, zi = interpolate.splev(ti,coef)
            Pi = np.array([xi,yi,zi]).T
            
            # concatenation
            if iseg==0:
                Pcrv = Pcrv + Pi.tolist()
            else:
                dx, dy, dz = Pcrv[-1] - Pi[0]
                Pt = points_transform(Pi,0.,0.,0.,dx,dy,dz)
                Pcrv = Pcrv + Pt.tolist()
    
            # save break index
            break_ids[iseg+1] = len(Pcrv)
            if self.seg_para[iseg]['lbl']=='plane':
                plane_ids.append([len(Pcrv)-npoints_seg,len(Pcrv)-1])
            elif self.seg_para[iseg]['lbl']=='line':
                line_ids.append([len(Pcrv)-npoints_seg,len(Pcrv)-1])
                
        # add noise to 3d curve
        Pcrv = np.array(Pcrv)
        Pcrv_noise = Pcrv + np.random.randn(len(Pcrv),3)*self.sigma
        
        self.coors = Pcrv
        self.coors_noise = Pcrv_noise
        self.break_ids = break_ids
        self.plane_ids = plane_ids
        self.line_ids = line_ids
        
        return self
                
    def get_curve(self,noisy=True):
        """Return curve with/without noise.
        """
        if noisy==True:
            return Curve(self.coors_noise)
        else:
            return Curve(self.coors)
        
    def plot(self,ax,projection='3d',noisy=True,scales=(1.,1.,1.)):
        
        if noisy==True:
            P = self.coors_noise
        else:
            P = self.coors
            
        pl.plot_point(ax,projection,P[0,0],P[0,1],P[0,2],scales,point_args={'c':'red','s':40})
        
        pl.plot_line(ax,projection,P[:,0],P[:,1],P[:,2],scales,line_args={'c':'green','alpha':0.7})
        
        if len(self.break_ids)>2:
            pl.plot_point(ax,projection,P[self.break_ids[1:-1],0],P[self.break_ids[1:-1],1],P[self.break_ids[1:-1],2],scales,point_args={'c':'magenta','s':20})
        for pid in self.plane_ids:
            pl.plot_line(ax,projection,P[pid[0]:pid[1],0],P[pid[0]:pid[1],1],P[pid[0]:pid[1],2],scales,line_args={'c':'yellow','alpha':0.7})

            
            if projection == '3d':
            
                data = P[pid[0]:pid[1]]
                c, normal = fit_plane(data)
                maxx = np.max(data[:,0])
                maxy = np.max(data[:,1])
                minx = np.min(data[:,0])
                miny = np.min(data[:,1])
                
                pnt = np.array([0.0, 0.0, c])
                d = -pnt.dot(normal)
                
                xx, yy = np.meshgrid([minx, maxx], [miny, maxy])
                z = (-normal[0]*xx - normal[1]*yy - d)*1. / normal[2]
                
                ax.plot_surface(xx, yy, z, color='yellow',alpha=0.4)
        
        for lid in self.line_ids:
            pl.plot_line(ax,projection,P[lid[0]:lid[1],0],P[lid[0]:lid[1],1],P[lid[0]:lid[1],2],scales,line_args={'c':'blue','alpha':0.7})
        
        if projection != '3d':
                ax.axis('equal')
        else:
            param = pl.fix_equal_axis(self.coors_noise / np.array(scales))
            ax.set_xlim(param['xmin'],param['xmax'])
            ax.set_ylim(param['ymin'],param['ymax'])
            ax.set_zlim(param['zmin'],param['zmax'])
    
        
        
                
        
        
        
        


        
        