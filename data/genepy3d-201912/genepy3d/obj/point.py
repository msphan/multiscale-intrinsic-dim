import numpy as np
import pandas as pd
import scipy.stats as scs
from sklearn.datasets.samples_generator import make_blobs
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from genepy3d.util import plot as pl
from CGAL.CGAL_Point_set_processing_3 import remove_outliers,jet_smooth_point_set
from pyevtk.hl import pointsToVTK
from CGAL.CGAL_Kernel import Point_3

class Points:
    """Points cloud in 3D.
    
    Attributes:
        pnt_arr (2d numpy array (float)): list of 3d points.
    
    """
    def __init__(self, _pnt_arr):
        self.pnt_arr = _pnt_arr
        
    def plot(self, ax, **kwds):
        """Plot points cloud.
        
        Args:
            ax: axis to be plotted.
            projection (str): support '3d'|'xy'|'xz'|'yz' plot.
            point_args (dic): matplotlib arguments for plotting points.
            equal_aspect (bool): make equal aspect for both axes.
        
        """
        
        if 'projection' in kwds.keys():
            projection = kwds['projection']
        else:
            projection = '3d'
        
        if 'equal_aspect' in kwds.keys():
            equal_aspect = kwds['equal_aspect']
        else:
            equal_aspect = True
            
        if 'point_args' in kwds.keys():
            point_args = kwds['point_args']
        else:
            point_args = {'color':'blue', 'alpha':0.9}

        x, y, z = self.pnt_arr[:,0], self.pnt_arr[:,1], self.pnt_arr[:,2]
        
        if projection == '3d':
            ax.scatter(x,y,z,**point_args)
            if equal_aspect == True:
                param = pl.fix_equal_axis(self.pnt_arr)
                ax.set_xlim(param['xmin'],param['xmax'])
                ax.set_ylim(param['ymin'],param['ymax'])
                ax.set_zlim(param['zmin'],param['zmax'])
            ax.set_xlabel('X')
            ax.set_ylabel('Y')
            ax.set_zlabel('Z')
        elif projection == 'xy':
            ax.scatter(x,y,**point_args)
            if equal_aspect == True:
                ax.axis('equal')
            ax.set_xlabel('X')
            ax.set_ylabel('Y')
        elif projection == 'xz':
            ax.scatter(x,z,**point_args)
            if equal_aspect == True:
                ax.axis('equal')
            ax.set_xlabel('X')
            ax.set_ylabel('Z')
        else:
            ax.scatter(y,z,**point_args)
            if equal_aspect == True:
                ax.axis('equal')
            ax.set_xlabel('Y')
            ax.set_ylabel('Z')
            
    def pca(self):
        """Principal components analysis of points cloud.
        
        Returns
            (1d numpy array) : empirical mean of 3d points.
            (2d numpy array) : components sorted by explained variance.
            (2d numpy array) : explained variance.
        
        """
        
        pca=PCA(n_components=3)
        pca.fit(self.__pnt_arr)
        return [pca.mean_,pca.components_,pca.explained_variance_]
    
    def estimate_orientation(self):
        """Give an estimation of the orientation of points cloud.
        
        Returns 
            orientations (list) : the spherical coordinates of each component given by pca.
           
        """
        
        mean,cp,vp=self.pca()
        orientations=[]
        # Here spherical coordianates are specified by
        # theta azimuthal angle, between the orthogonal projection(on Oxy plan) and Ox axis
        # phi polar angle, between the component and Oz axis
        for u in cp :
            if np.abs(u[0])<0.1 :
                if np.abs(u[1])<0.1 :
                    phi=0
                    # if phi equals to zero then the component is collinear to Oz axis and
                    # theta has no numerical value
                    theta='theta'
                else :
                    theta=np.pi/2
                    phi=np.arccos(u[2])
            else :
                phi=np.arccos(u[2])
                theta=np.arctan(u[1]/u[0])
            orientations.append([theta,phi])
        return orientations
    
    def isotropy_test(self,n_iter=100):
        """Statistical test for isotropy.
        
        Args
            n_iter (int) : number of iterations.
        
        Returns
            p_value (float) : the points cloud is anisotropic if only
            p_value < alpha (where alpha is a test level, e.g: alpha=0.05).
        
        """
        
        # PCA to collect explained variance
        pca=PCA(n_components=3)
        pca.fit(self.__pnt_arr)
        vp1,vp2,vp3=pca.explained_variance_
        # vp1/(vp1+vp2+vp3) proportion is near to 1/3 if the points cloud is isotropic.
        # Another proportion of the same type can be calculated with a spherical points
        # cloud which is a model of isotropic points cloud.
        # Simulating and calculating the second proportion several times allows to know
        # how much the studied sample is near to the model.
        t=vp1/(vp1+vp2+vp3)
        n_isotrop=0.0
        for m in range(n_iter):
            n=len(self.__pnt_arr)
            sphere=simulate_sphere(n,1)
            pca.fit(sphere.__pnt_arr)
            ev1,ev2,ev3=pca.explained_variance_
            tprime=ev1/(ev1+ev2+ev3)
            if t<=tprime :
                n_isotrop+=1
        p_value=n_isotrop/n_iter
        return p_value
    
    def kmeans(self,**kwargs):
        """Find the clusters of points cloud with the kmeans algorithm.
        
        Args 
            **kwargs : KMeans keyword arguments in scikitlearn.
                                
        Returns 
            labels (1d numpy array) : the integer label of each sample's point.
            centers(2d numpy array) of shape (n_clusters,n_features) : the center of each cluster.
        
        """
        
        kmeans=KMeans(**kwargs)
        kmeans.fit(self.__pnt_arr)
        labels=kmeans.predict(self.__pnt_arr)
        centers=kmeans.cluster_centers_
        return [labels,centers]
    def to_CGAL(self):
        """return the points as a list of CGAL Point_3 objects, for use in CGAL aglorithms
                                
        Returns 
            a list of CGAL.Point_3 objects
        
        """
        
         #import CGAL? needs to be done from the caller, TODO: test that it's done
        return [Point_3(self.pnt_arr[i,0],self.pnt_arr[i,1],self.pnt_arr[i,2]) for i in range(len(self.pnt_arr))]

    def processs(self,removed_percentage = 5.0,nb_neighbors = 24, smooth=True):

        pointsCgal=self.toCGAL()
         
        if removed_percentage:
            new_size=remove_outliers(pointsCgal, nb_neighbors, removed_percentage)
            pointsCgal=pointsCgal[0:new_size]
            
        if smooth:
            jet_smooth_point_set(pointsCgal,nb_neighbors)

        self.pnt_arr = np.array([[p.x(),p.y(),p.z()] for p in pointsCgal])

    def center(self):
        """center the cloud to the mean of each coordinates
                                                
        """
        self.pnt_arr[:,1]=self.pnt_arr[:,1]-self.pnt_arr[:,1].mean()
        self.pnt_arr[:,2]=self.pnt_arr[:,2]-self.pnt_arr[:,2].mean()
    
    def export_to_VTK(self,filepath):
        """Find the clusters of points cloud with the kmeans algorithm.
        
        Args 
            filepath : full path of file to save, without extention
                                        
        """

        pointsToVTK(filepath, np.ascontiguousarray(self.pnt_arr[:,0]),  np.ascontiguousarray(self.pnt_arr[:,1]),  np.ascontiguousarray(self.pnt_arr[:,2]),data=None)


### END points object

    
def read_csv(file_path):
    """Generate 3d points with datas inside a file.
    
    Args
        file_path (str) : directory and name of a file.
    
    Returns 
        (Points object) created with file's datas.
    
    """
    data=pd.read_csv(file_path,header=None)
    pts=data.values
    return Points(pts)

def __rotation_matrix(u,theta):
    """Give the rotation matrix relative to a direction and an angle.
    
    Args
        u (1d numpy array) : 3d vector for rotation direction.
        theta (float) : rotation angle in radians.
        
    Returns
        (2d numpy array of shape (3,3)) : rotation matrix.
    
    
    """
    P=np.kron(u,u).reshape(3,3)
    Q=np.diag([u[1]],k=2)+np.diag([-u[2],-u[0]],k=1)+np.diag([u[2],u[0]],k=-1)+np.diag([-u[1]],k=-2)
    return P+np.cos(theta)*(np.eye(3)-P)+np.sin(theta)*Q

def generate_ellipsoid(axes_length=[1.,1.,1.],dtheta=np.pi/20,dphi=np.pi/20):
    """Generate point cloud on the surface of an ellipsoid. !! As genetared, the points are not equally spaced.
    
    Args:
        axes_length: half length of the main axis along the three axes ([1.,1.,1.]) 
        dtheta: sampling along theta (np.pi/20)
        dphi: sampling along phi (np.pi/20)
    
    Returns :
        (Points object) created with the generated sample.
    
    """

    a=axes_length[0]
    b=axes_length[0]
    c=axes_length[0]
    
    theta=np.arange(0,2*np.pi,dtheta)
    phi=np.arange(0,2*np.pi,dphi)
    xel=[a*np.sin(t)*np.cos(p) for t in theta for p in phi]
    yel=[b*np.sin(t)*np.sin(p) for t in theta for p in phi]
    zel=[c*np.cos(t) for t in theta for p in phi]
    
    return Points(np.array([xel,yel,zel]).transpose())
 
def simulate(n=1000,center=[0,0,0],scales=1,orientation=[0,np.pi/2]):
    """Generate gaussian sample of 3d points with a certain orientation.
    
    Args
        n (int) : sample's size.
        scales (int, float, 1d numpy array or list) : standard deviations.
        orientation (array or list) : spherical coordinates of the first component given by pca.
    
    Returns 
        (Points object) created with the generated sample.
    
    """
    
    # Simulation of a gaussian sample whose components are exactly
    # the canonical vectors
    X=scs.norm.rvs(loc=center,scale=scales,size=(n,3))
    theta,phi=orientation
    # Getting the asked orientation after some rotations
    R=__rotation_matrix([0,0,1],theta)
    v=np.cross(np.dot(R,[1,0,0]),[0,0,1])
    R=np.dot(__rotation_matrix(v,np.pi/2-phi),R)
    return Points(np.dot(X,R.transpose()))

#def simulate_2(n,scales,angles):
#    """Generate gaussian sample of 3d points    
#    
#    Args : n: sample's size
#           scales: (array or list) standard deviations
#           angles: (array or list) 
#    
#    Returns : (Points object) created with the generated sample
#    
#    """
#    
#    X=scs.norm.rvs(scale=scales,size=(n,3))
#    R=np.eye(3)
#    for u,theta in zip(np.eye(3),angles):
#        R=np.dot(__rotation_matrix(u,theta),R)
#    return Points(np.dot(X,R.transpose()))

def simulate_sphere(n=1000,rho=1):
    """Generate 3d points uniformly on a sphere.
    
    Args
        n (int) : sample's size.
        rho (int or float) : sphere's radius.
        
    Returns
        (Points object) created with the generated sample.
    
    """
    
    #Simulation with spherical coordinates
    thetas=np.arccos(-2*np.random.random_sample(n)+1)  
    phis=2*np.pi*np.random.random_sample(n)
    pts=np.zeros((n,3))
    # theta polar angle
    # phi azimuthal angle
    for theta,phi,i in zip(thetas,phis,np.arange(n)):
        pts[i]=[rho*np.sin(theta)*np.cos(phi),rho*np.sin(theta)*np.sin(phi),rho*np.cos(theta)]
    return Points(pts)

def simulate_blobs(**kwargs):
    """Generate isotropic gaussian blobs for clustering.
    
    Args 
        **kwargs : make_blobs keyword agruments in scikitlearn.
        
    Returns
        (Points object) created with the generated sample.
        labels (1d numpy array) : the integer label of each sample's point.
    
    """
    
    pts,labels=make_blobs(**kwargs)
    return [Points(pts),labels]
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
