import numpy as np
import pandas as pd
import anytree
from anytree import search, PreOrderIter
from anytree.walker import Walker
import matplotlib
from genepy3d.util import plot as pl

class Tree:
    """Tree in 3D. Support for manipulating neurons.
    
    Attributes
        tree_name (str)
        tree_id (int)
        dftree (pandas dataframe): tree data table.
        dfcon (pandas dataframe): connector data table.
        
    columns of treetbl
        - treenode_id (int)
        - structure_id (int)
        - x, y, z (float)
        - r (float)
        - parent_treenode_id (int)
        
    columns of contblb
        - connector_id
        - treenode_id
        - relation_id
    
    """
    
    def __init__(self,**kwds):
        """Initialize tree attributes.
        
        Args
            tree_id or neuron_id (int): tree id, can be empty.
            tree_name or neuron_name (str): tree name, can be empty.
            dftree or dfneu (dataframe): tree data.
            dfcon (dataframe): connector data, can be empty.
            
        """
        
        # check tree_id by orders
        if 'neuron_id' in kwds.keys():
            self.tree_id = kwds['neuron_id']
        elif 'tree_id' in kwds.keys():
            self.tree_id = kwds['tree_id']
        else:
            self.tree_id = 0 # default value

        # check tree_name            
        if 'neuron_name' in kwds.keys():
            self.tree_name = kwds['neuron_name']
        elif 'tree_name' in kwds.keys():
            self.tree_name = kwds['tree_name']
        else:
            self.tree_name = 'GeNePy3D' # default value
            
        # check dftree
        if 'dfneu' in kwds.keys():
            dftree = kwds['dfneu']
        elif 'dftree' in kwds.keys():
            dftree = kwds['dftree']
        else:
            raise ValueError('can not find "dfneu" or "dftree".')
        
        if dftree is None:
            raise ValueError('"dftree" is empty.')
        elif len(dftree)==0:
            raise ValueError('"dftree" is empty.')
        
        # check dftree columns
        if isinstance(dftree,pd.DataFrame):
            lbls = dftree.columns.values
            if all(lbl in lbls for lbl in ['treenode_id','structure_id','x','y','z','r','parent_treenode_id']):
                dftree.dropna(inplace=True)
                self.dftree = dftree.copy()
            else:
                raise Exception("can not find 'treenode_id','structure_id','x','y','z','r','parent_treenode_id' within the dataframe.")
        else: 
            raise Exception("dftree must be pandas dataframe.")
        
        # constraint some columns of dftree to int
        self.dftree['structure_id'] = self.dftree['structure_id'].astype('int')
        self.dftree['treenode_id'] = self.dftree['treenode_id'].astype('int')
        self.dftree['parent_treenode_id'] = self.dftree['parent_treenode_id'].astype('int')
        self.dftree.set_index('treenode_id',inplace=True) # resetting dataframe index
        
        # check dfcon
        if 'dfcon' in kwds.keys():
            dfcon = kwds['dfcon']
        else:
            dfcon = None
        
        self.dfcon = None
        if dfcon is not None:
            # check dfcon columns
            if len(dfcon)>0:
                if isinstance(dfcon,pd.DataFrame):
                    
                    lbls = dfcon.columns.values
                    if all(lbl in lbls for lbl in ['connector_id','treenode_id','relation_id']):
                        dfcon.dropna(inplace=True)
                        self.dfcon = dfcon.copy()
                    else:
                        raise Exception("can not find 'connector_id','treenode_id','relation_id' within the dataframe.")
                else:
                    raise Exception("dfcon must be pandas dataframe.")
                
                # constraint columns to int
                self.dfcon['connector_id'] = self.dfcon['connector_id'].astype('int')
                self.dfcon['treenode_id'] = self.dfcon['treenode_id'].astype('int')
        
        # build tree structure
        self._build_treenodes()
        
        # some summary
        self._short_summary()
               
    def _build_treenodes(self):
        """Building tree object using anytree lib.
        """        
        
        self.treenodes = {}
        self.treefeatures = {}
        
        # get connector nodes
        conlst = []
        if self.dfcon is not None:
            conlst = self.dfcon['treenode_id'].unique().astype('int') # it may happen similar (ske,treenode) for 2 different connector id 
        self.treefeatures['connectors'] = conlst
        
        # build treenodes
        for i in range(len(self.dftree)):
            nodeid = int(self.dftree.index[i])
            if nodeid in conlst:
                self.treenodes[nodeid] = anytree.Node(nodeid,isconnector=True)
            else:
                self.treenodes[nodeid] = anytree.Node(nodeid,isconnector=False)
        for i in range(len(self.dftree)):
            nodeid = int(self.dftree.index[i])
            parentnodeid = int(self.dftree.iloc[i]['parent_treenode_id'])
            if parentnodeid != -1:
                self.treenodes[nodeid].parent = self.treenodes[parentnodeid]
            else:
                self.treefeatures['root'] = nodeid
                
        # compute some features
        
        rootid = self.treefeatures['root']
        
        # leaves
        self.treefeatures['leaves'] = [leaf.name for leaf in self.treenodes[rootid].leaves]
        
        # internodes
        self.treefeatures['internodes'] = [n.name for n in search.findall(self.treenodes[rootid],filter_=lambda node: (len(node.children)>1) & (node.name!=rootid))]
        
        # spine (longest branch)
        leaf_depth = np.array([[n.depth, n.name] for n in self.treenodes[rootid].leaves])
        longest_branch_nodes = self.treenodes[leaf_depth[np.argmax(leaf_depth[:,0]),1]]
        self.treefeatures['spine'] = [n.name for n in longest_branch_nodes.path]
        
        # segments
        segments = []
        controlnodes = self.treefeatures['leaves'] + self.treefeatures['internodes'] + [rootid]
        for it in range(len(controlnodes)):
            node = controlnodes[it]
            branch = [n.name for n in self.treenodes[node].path] 
            idx = len(branch)-2
            while(idx != -1):
                if branch[idx] in controlnodes:
                    subbranch = branch[idx:]
                    segments.append(subbranch)
                    break
                idx = idx - 1
        self.treefeatures['segments'] = segments
        
        # strahler order
        preorder_lst = np.array([node.name for node in PreOrderIter(self.treenodes[rootid])])
        for i in preorder_lst[-1::-1]:
            n = self.treenodes[i]
            if n.is_leaf:
                n.strahler = 1
            else:
                children_strahler = np.array([[k.name, k.strahler] for k in n.children])
                if len(children_strahler)==1:
                    n.strahler = children_strahler[0,1]
                else:
                    nbmax = len(np.argwhere(children_strahler[:,1]==np.max(children_strahler[:,1])).flatten())
                    if nbmax>=2:
                        n.strahler = np.max(children_strahler[:,1]) + 1
                    else:
                        n.strahler = np.max(children_strahler[:,1])
                        
    def _short_summary(self):
        
        s = {}
        s['nb_nodes'] = len(self.dftree)
        s['nb_leaves'] = len(self.treefeatures['leaves'])
        s['nb_internodes'] = len(self.treefeatures['internodes'])
        s['nb_connectors'] = len(self.treefeatures['connectors'])
        s['nb_segments'] = len(self.treefeatures['segments'])
        s['nb_spine_nodes'] = len(self.treefeatures['spine'])
        self.treesummary = s
        
    def get_root(self):
        
        return self.treefeatures['root']
    
    def get_leaves(self):
        
        return self.treefeatures['leaves']
    
    def get_connectors(self):
        
        return self.treefeatures['connectors']
    
    def get_internodes(self):
        
        return self.treefeatures['internodes']
    
    def get_spine(self):
        
        return self.treefeatures['spine']
    
    def get_segments(self):
        
        return self.treefeatures['segments']
    
    def get_strahler_order(self,node_id=None):
        """Return strahler order for given node_id.
        
        Args
            node_id (int|[int]): list of node_id.
        
        Returns
            Pandas serie of strahler order.
        
        """
        
        if node_id is None:
            nodelst = list(self.dftree.index.values)
        elif node_id is not None:
            if isinstance(node_id,int): # only one item.
                nodelst = [node_id]
            elif isinstance(node_id,(list,np.ndarray)):
                nodelst = node_id
            else:
                raise Exception("node_id must be array-like.")
                
        strahler_orders = [self.treenodes[i].strahler for i in nodelst]
        return pd.Series(strahler_orders,name='strahler_order',index=nodelst)
    
    def get_coordinates(self,nodes=None):
        if nodes is None:
            points = self.dftree[['x','y','z']].values
            return (points[:,0], points[:,1], points[:,2])
        elif isinstance(nodes,int): # only one item.
            points = self.dftree.loc[nodes][['x','y','z']].values
            return (points[0], points[1], points[2])
        elif isinstance(nodes,(list,np.ndarray)):
            points = self.dftree.loc[nodes][['x','y','z']].values
            return (points[:,0], points[:,1], points[:,2])
        else:
            raise Exception("nodes must be array-like.")
    
    def path(self,target,source=None):
        """Find list of nodes passing from source node to target node.
        
        Args
            target (int): target node id.
            source (int): source node id.
            
        Returns
            list of passed nodes included source and target.
        
        """
        
        
        if source==None:
            source = self.get_root()
            
        w = Walker()
        res = w.walk(self.treenodes[source],self.treenodes[target])
        return [n.name for n in res[0]] + [res[1].name] + [n.name for n in res[2]] 
    
    def extract_subtrees(self,node,to_children=True,separate_children=False):
        """Extract a sub tree from the given node.
        
        Args:
            node (int): node that becomes new root node in the sub tree.
            to_children (bool): if False, then extract upper subtree, else lower subtrees are extracted.
            separate_children (bool): if True, then different subtrees are extracted from children. Only available if to_children = True.
        
        Returns:
            new trees.
        
        """
        
        # extract sub tree
        nodelst = [node] + [item.name for item in self.treenodes[node].descendants]
        
        if to_children == False:
            rootnode = self.get_root()
            fullnodelst = [rootnode] + [item.name for item in self.treenodes[rootnode].descendants]
            nodelst = np.setdiff1d(fullnodelst,nodelst)
            subdftree = self.dftree.loc[nodelst].copy()
        else:
            if separate_children==True:
                mychildren = [item.name for item in self.treenodes[node].children]
                if len(mychildren)>1:
                    data = []
                    for mychild in mychildren:
                        subnodelst = [node, mychild] + [item.name for item in self.treenodes[mychild].descendants]
                        subdftree = self.dftree.loc[subnodelst].copy()  
                        subdftree.at[node,'parent_treenode_id']=-1 # set new root node
                        subdftree.reset_index(inplace=True)
                        
                        subdfcon = None
                        if self.dfcon is not None:
                            subdfcon = self.dfcon[self.dfcon['treenode_id'].isin(subnodelst)].copy()
                            if len(subdfcon)==0:
                                subdfcon = None
                                
                        data.append(Tree(tree_id=self.tree_id,tree_name=self.tree_name,dftree=subdftree,dfcon=subdfcon))
                        
                    return data
            
            subdftree = self.dftree.loc[nodelst].copy()  
            subdftree.at[node,'parent_treenode_id']=-1 # set new root node
        
        
        subdftree.reset_index(inplace=True)
        
        # extract sub connectors
        subdfcon = None
        if self.dfcon is not None:
            subdfcon = self.dfcon[self.dfcon['treenode_id'].isin(nodelst)].copy()
            if len(subdfcon)==0:
                subdfcon = None
        
        return Tree(tree_id=self.tree_id,tree_name=self.tree_name,dftree=subdftree,dfcon=subdfcon)
    
    
    def decompose_spines(self):
        """Decompose tree into list of spines.
        
        Returns:
            list of spines nodes.
        
        """
        
        data = {}
        
        # first check if there're many branches at root
        rootnode = self.get_root()
        mychildren = [item.name for item in self.treenodes[rootnode].children]
        if len(mychildren)>1: # more than one children
            subtrees = self.extract_subtrees(rootnode,separate_children=True)
            for subtree in subtrees:
                subdata = subtree.decompose_spines()
                data = {**data, **subdata}
        else:
            spinenodes = self.get_spine()
            spinename = str(spinenodes[0])+'-'+str(spinenodes[1])
            data[spinename] = spinenodes
            internodes = self.get_internodes()
            if len(internodes)!=0:
                spineinternodes = list(filter(lambda node : node in spinenodes, internodes))
                
                for node in spineinternodes:
                    subtrees = self.extract_subtrees(node=node,separate_children=True)
                    for subtree in subtrees:
                        firstchild = subtree.treenodes[subtree.get_root()].children[0].name
                        if firstchild not in spinenodes:
                            subdata = subtree.decompose_spines()
                            data = {**data, **subdata}
                
        return data
    
    def summary(self):
        
        index = ['id','name','root','nb_nodes','nb_leaves','nb_internodes','nb_connectors','nb_segments','nb_spine_nodes']
        data = [self.tree_id,self.tree_name,
                self.treefeatures['root'],self.treesummary['nb_nodes'],self.treesummary['nb_leaves'],
                self.treesummary['nb_internodes'],self.treesummary['nb_connectors'],self.treesummary['nb_segments'],self.treesummary['nb_spine_nodes']]
        return pd.Series(data,index=index)
    
    def plot(self,ax,projection='3d',spine_only=False,
             show_root=True,show_leaves=True,show_internodes=True,show_connectors=True,
             root_args={'s':50,'c':'red'},leaves_args={'s':8,'c':'blue'},internodes_args={'s':20,'c':'magenta'},connectors_args={'s':70,'c':'red','alpha':0.7},
             show_strahler=False,segments_colors='black',
             line_args={'alpha':0.8},scales=(1.,1.,1.),cmap='viridis',equal_axis=True):
        
        if show_root==True:
            root_node = self.get_root()
            x, y, z = self.get_coordinates(root_node)
            pl.plot_point(ax,projection,x,y,z,scales,root_args)
        
        if spine_only==True:
            
            spine_nodes = self.get_spine()
            x, y, z = self.get_coordinates(spine_nodes)
            pl.plot_line(ax,projection,x,y,z,scales,line_args)
            
            if show_leaves==True:
                x, y, z = self.get_coordinates(spine_nodes[-1])
                pl.plot_point(ax,projection,x,y,z,scales,leaves_args)
                
        else:
            
            segments = self.get_segments()
            for seg_nodes in segments:
                x, y, z = self.get_coordinates(seg_nodes)
                segment_args = line_args.copy()
                
                if segments_colors=='random':
                    mycmap = matplotlib.cm.get_cmap(cmap)
                    segment_args['c']= mycmap(np.random.rand())
                else:
                    segment_args['c'] = segments_colors
                    
                if show_strahler==True:
                    order = self.get_strahler_order(seg_nodes).min()
                    segment_args['lw'] = order
                pl.plot_line(ax,projection,x,y,z,scales,segment_args)
            
            if show_leaves==True:
                leaves_nodes = self.get_leaves()
                x, y, z = self.get_coordinates(leaves_nodes)
                pl.plot_point(ax,projection,x,y,z,scales,leaves_args)
                
            if show_internodes==True:
                inter_nodes = self.get_internodes()
                if len(inter_nodes)!=0:
                    x, y, z = self.get_coordinates(inter_nodes)
                    pl.plot_point(ax,projection,x,y,z,scales,internodes_args)
                    
            if show_connectors==True:
                connectors_nodes = self.get_connectors()
                if len(connectors_nodes)!=0:
                    x, y, z = self.get_coordinates(connectors_nodes)
                    pl.plot_point(ax,projection,x,y,z,scales,connectors_args)
                
        if equal_axis==True:
            if projection != '3d':
                ax.axis('equal')
            else:
                param = pl.fix_equal_axis(self.dftree[['x','y','z']].values / np.array(scales))
                ax.set_xlim(param['xmin'],param['xmax'])
                ax.set_ylim(param['ymin'],param['ymax'])
                ax.set_zlim(param['zmin'],param['zmax'])
                
        if projection != '3d':
            if projection=='xy':
                xlbl, ylbl = 'X', 'Y'
            elif projection=='xz':
                xlbl, ylbl = 'X', 'Z'
            else:
                xlbl, ylbl = 'Y', 'Z'
            ax.set_xlabel(xlbl)
            ax.set_ylabel(ylbl)
        else:
            ax.set_xlabel('X')
            ax.set_ylabel('Y')
            ax.set_zlabel('Z')
            
                    
            
                
            
            
            
        
        
        
        
        
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
        
        
        
        
