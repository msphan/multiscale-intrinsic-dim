#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# TODO build_skeleton_db(): should add x, y and z scales as input parameters?
# TODO throw execption when build skedb for a same db name?

import numpy as np
import pandas as pd
import anytree

from genepy3d.io import catmaid as cat

class H5Db:
    """HDF5 database for annotated dataset.
    
    The annotated dataset contains skeleton-like structure used to annotate e.g. the axons
    and can be done by using CATMAID. This class supports building HDF5 database from CATMAID
    dataset via CATMAID API and allowing to query from the database.
    
    Dataframes in database:
        
    - dfske: skeleton table.
    - dfcon: connector table.
    - dfsyn: synapses table.
    - dfsketree: table contains tree structures of skeletons.
    
    Attributes:
        dbname (str): hdf5 file path.
        
    Note:
        If the Hdf5 file doesn't exist, then use build_skeleton_db() to build database.
    
    """
    def __init__(self,dbname):
        """Set Hdf5 file path.
        
        Args:
            dbname (str): hdf5 file path.
        
        """
        
        self.dbname = dbname
        
    def get_dataframe(self,name,columns=None,query=None):
        """Return a dataframe, reuse select() method in HDFStore class of Pandas library.
        
        Args:
            name (str): table name.
            columns (list [str]): list of columns names. If None, then all columns are loaded.
            query (str): query string.
            
        Returns:
            Pandas dataFrame.
        
        """
        store = pd.HDFStore(self.dbname,mode='r')
        df = store.select(name,where=query,columns=columns)   
        store.close()
        return df
    
    def get_skeleton_id(self,relation='presynaptic_to',nodes=1,main_nodes=1,is_branch=False):
        """Return skeleton indices for given conditions.
        
        Args:
            relation (str): relation type. Should be 'presynaptic_to', 'postsynaptic_to' or None.
            nodes (int): minimal number of nodes on the skeleton.
            main_nodes (int): minimal number of nodes on the main branch of the skeleton.
            is_branch (bool): if True, then only take the skeletons having branches.
            
        Returns:
            Numpy array [int] of skeleton indices that are sorted by ascending order.
        
        """
        dfske = self.get_dataframe('dfske')
        dfsketree = self.get_dataframe('dfsketree')
        dfcon = self.get_dataframe('dfcon')
        
        # initial skeleton ids
        skelst = dfske['skeleton_id'].unique()
        
        # filter by relation
        if((relation=='presynaptic_to') | (relation=='postsynaptic_to')):
            skelst_rela = dfcon[dfcon['relation_id']==relation]['skeleton_id'].unique()
            skelst = np.intersect1d(skelst,skelst_rela)
        
        # filter by number of nodes minimum
        dfske_counts = dfske['skeleton_id'].value_counts()
        skelst_nodes = dfske_counts[dfske_counts>=nodes].index.values
        skelst = np.intersect1d(skelst,skelst_nodes)
        
        # filter by number of nodes minimum of the main branch
        if main_nodes >= 2:
            mainbranch_counts = dfsketree[dfsketree['category']==3]['skeleton_id'].value_counts()
            skelst_mainnodes = mainbranch_counts[mainbranch_counts>=main_nodes].index.values
            skelst = np.intersect1d(skelst,skelst_mainnodes)
        
        # filter by branch
        if is_branch==True:
            skelst_branched = dfsketree[dfsketree['category']>3]['skeleton_id'].unique()
            skelst = np.intersect1d(skelst,skelst_branched)
            
        return np.sort(skelst)
    
    def get_connectors(self,ske_lst):
        """Return a dataframe with connectors info.            
        
        Args:
            ske_lst (array-like [int]): skeleton indices.
            
        Returns:
            Pandas dataframe.
            
        Note:
            This simply show connector info for each skeleton (either pre or post). It does not mean that the connector must etablish a synapse.
        
        """
        dfcon = self.get_dataframe('dfcon')
        
        if len(dfcon)==0:
            return 
        else:
            dfske = self.get_dataframe('dfske')
    
            subdf = dfcon[dfcon['skeleton_id'].isin(ske_lst)]
            subdf = subdf.sort_values(['skeleton_id'])
            
            subdf = subdf.drop_duplicates(['skeleton_id','treenode_id'])
            
            mergedf = pd.merge(subdf,dfske,on=['skeleton_id','treenode_id'],how='inner')
            is_root = mergedf['parent_treenode_id'].isnull()
            mergedf['is_root']=is_root
            
            df = mergedf[['connector_id','skeleton_id','treenode_id','is_root','x','y','z']].copy()
            df = df.set_index(['skeleton_id','treenode_id'])
            
            return df

    def get_pre_multipost(self,pre_ske_lst):
        """Return a dataframe specifying the cases that one presynaptic-to neuron has multiple postsynpatic-to neurons.
        
        Args:
            pre_ske_lst (array-like [int]): presynaptic-to skeleton indices.
            
        Return:
            Pandas dataframe.
        
        """
        dfske = self.get_dataframe('dfske')
        dfsyn = self.get_dataframe('dfsyn')
        dfsyn = dfsyn[dfsyn['pre_skeleton_id'].isin(pre_ske_lst)]
        
        subdf = dfsyn.sort_values(['pre_skeleton_id'])
        subdf_counts = subdf.groupby(['pre_skeleton_id'])['post_skeleton_id'].count()
        subskelst = subdf_counts[subdf_counts>=2].index.values
        
        subdf = subdf[subdf['pre_skeleton_id'].isin(subskelst)]
        subdf.columns = ['skeleton_id','treenode_id','post_skeleton_id','post_treenode_id']
        
        mergedf = pd.merge(subdf,dfske,on=['skeleton_id','treenode_id'],how='inner')
        is_root = mergedf['parent_treenode_id'].isnull()
        mergedf['is_root']=is_root
        
        df = mergedf[['skeleton_id','post_skeleton_id','post_treenode_id','treenode_id','is_root','x','y','z']].copy()
        df.set_index(['skeleton_id','post_skeleton_id'],inplace=True)
        df.index.rename('pre_skeleton_id',level=0,inplace=True)
        df.rename(columns={'treenode_id':'pre_treenode_id'},inplace=True)
        
        return df
        
    def get_pre_multiinnerv(self,pre_ske_lst):
        """Return a dataframe specifying the cases that multiple presynaptic-to neurons innervate one postsynaptic-to neuron.
        
        Args:
            pre_ske_lst (array-like [int]): presynaptic-to skeleton indices.
            
        Return:
            Pandas dataframe.
        
        """
        dfske = self.get_dataframe('dfske')
        dfsyn = self.get_dataframe('dfsyn')
        
        subdf = dfsyn[dfsyn['pre_skeleton_id'].isin(pre_ske_lst)]
        subdf = subdf.sort_values(['post_skeleton_id'])
        
        subdf_counts = subdf.groupby(['post_skeleton_id'])['pre_skeleton_id'].count()
        subskelst = subdf_counts[subdf_counts>=2].index.values
        
        subdf = subdf[subdf['post_skeleton_id'].isin(subskelst)]
        subdf.columns = ['skeleton_id','treenode_id','post_skeleton_id','post_treenode_id']
        
        mergedf = pd.merge(subdf,dfske,on=['skeleton_id','treenode_id'],how='inner')
        is_root = mergedf['parent_treenode_id'].isnull()
        mergedf['is_root']=is_root
        
        df = mergedf[['post_skeleton_id','skeleton_id','treenode_id','is_root','x','y','z','post_treenode_id']].copy()
        df = df.set_index(['post_skeleton_id','skeleton_id'])
        df.index.rename('pre_skeleton_id',level=1,inplace=True)
        df.rename(columns={'treenode_id':'pre_treenode_id'},inplace=True)
                
        return df
    
    def _geolen(self,coors):
        """Calculate geodesic length from list of coordinates.
        
        Args:
            coors (2d numpy array [float]): 3d coordinates.
            
        Returns:
            Geodesic length.
        
        """
        l = 0.
        for i in range(len(coors)-1):
            a = coors[i]
            b = coors[i+1]
            l = l + np.sqrt(np.sum((b-a)**2))
        return l          
    
    def merge_feature(self,ske_lst,feature_lst):
        """Merge features into a skeleton dataframe.
        
        Given additional features of skeleton table such as diameter, color, etc.
        The function returns a dataframe that merge these new features to the skeleton table.
        
        Args:
            ske_lst (array-like [int]): skeleton indices to be merged.
            feature_lst (array-like [tuple]): description for features to be merged. 
            
                E.g. feature_lst = [('path', 'name', 'newname')] where
                
                - path: feature path.
                - name: name of subfeature.
                - newname: new name of the subfeature to be merged.
    
        Returns:
            New Pandas dataframe with merged features.
            
        """
        dfske = self.get_dataframe('dfske')
        dfske = dfske[dfske['skeleton_id'].isin(ske_lst)].copy() # get only subdataframe containing ske_lst
        
        # initialize new data to be merged
        newdata = dict()
        for item in feature_lst:
            for name in item[2]:
                newdata[name] = []
        
        # get data from feature_path and put into newdata 
        for skeid in dfske['skeleton_id'].unique():
            for item in feature_lst:  
                feature_path, feature_name_lst, new_name_lst = item[0], item[1], item[2]
                data = np.load(feature_path+str(skeid)+'.npy').item()
                for i in range(len(feature_name_lst)):
                    newdata[new_name_lst[i]] = newdata[new_name_lst[i]] + data[feature_name_lst[i]].tolist()
                
        # merge into dfske
        for key,value in newdata.iteritems():
            dfske[key] = value
            
        return dfske        
    
    def build_skeleton_db(self,**kwds):
        """Build database.
        
        We support for instance two options when building database:
        
        - Building from CATMAID: need to specify catmaid_host, token and the project_id.
        - Building from files: need to specify ske_csv and con_csv.
            
        Args:
            catmaid_host (str): CATMAID host.
            token (str): authentication token string.
            project_id (int): project id.
            skeletonlst (list): list of skeleton id.
            ske_csv (str): skeletons csv file. It can be obtained by CATMAID exporter.
            con_csv (str): connector csv file. It can be obtained by CATMAID exporter.

        Returns:
            A Hdf5 file is created containing skeletons, skeletons trees, connectors, synapses tables
               
        Note: 
            - If the dataframes already existed in the database, they will be overwritten.   
            - For instance, we should not overwrite the dataframe, let's build a new database instead.
            
        """
        if all(key in kwds.iterkeys() for key in ['catmaid_host','token','project_id']) and (len(kwds)==3):
            catmaid_host = kwds['catmaid_host']
            token = kwds['token']
            project_id = kwds['project_id']
            catmaid = cat.CatmaidWrapper(catmaid_host,token)
        
            if 'skeletonlst' in kwds.keys():
                skeletonlst = kwds['skeletonlst']
            else:
                skeletonlst = catmaid.get_skeleton_list(project_id)
            
            skeinfo,coninfo = catmaid.get_skeleton_list_detail(project_id,skeletonlst)
            syninfo = cat.compute_synapse(coninfo)
            
            dfske = pd.DataFrame(skeinfo,columns=['skeleton_id','treenode_id','structure_id','x','y','z','r','parent_treenode_id'])
            dfcon = pd.DataFrame(coninfo,columns=['skeleton_id','connector_id','treenode_id','relation_id'])
            dfsyn = pd.DataFrame(syninfo,columns=['pre_skeleton_id','pre_treenode_id','post_skeleton_id','post_treenode_id'])
        
        elif all(key in kwds.iterkeys() for key in ['ske_csv','con_csv']) and (len(kwds)==2):
            
            dfske = pd.read_csv(kwds['ske_csv'])
            labels = dfske.columns.values
            refined_labels = []
            for lbl in labels:
                refined_labels.append(lbl.split()[0].lower())
            if all(lbl in refined_labels for lbl in ['skeleton_id','treenode_id','parent_treenode_id','x','y','z','r']):
                dfske.columns = refined_labels
                dfske['structure_id'] = 0
                dfske = dfske[['skeleton_id','treenode_id','structure_id','x','y','z','r','parent_treenode_id']]
                
            else:
                raise ValueError("can not find 'skeleton_id','treenode_id','parent_treenode_id','x','y','z','r' columns in ske_csv file")
                
            dfcon = pd.read_csv(kwds['con_csv'])
            labels = dfcon.columns.values
            refined_labels = []
            for lbl in labels:
                refined_labels.append(lbl.split()[0].lower())
            if all(lbl in refined_labels for lbl in ['connector_id','skeleton_id','treenode_id','relation_id']):
                dfcon.columns = refined_labels
                dfcon = dfcon[['skeleton_id','connector_id','treenode_id','relation_id']]
            else:
                raise ValueError("can not find 'connector_id','skeleton_id','treenode_id','relation_id' columns in con_csv file")
                
            
            syninfo = cat.compute_synapse(dfcon)
            dfsyn = pd.DataFrame(syninfo,columns=['pre_skeleton_id','pre_treenode_id','post_skeleton_id','post_treenode_id'])
        
        try:
            # drop duplicates
            dfske = dfske.drop_duplicates()
            dfcon = dfcon.drop_duplicates()
            dfsyn = dfsyn.drop_duplicates()
            
            self._append('dfske',dfske)
            self._append('dfcon',dfcon)
            self._append('dfsyn',dfsyn)
            try:
                dfsketree = self._build_skeleton_tree(dfske,dfcon) # build skeleton tree
            except:
                raise Exception('Failed when creating skeleton tree structure.')
            
            # drop duplicates
            dfsketree = dfsketree.drop_duplicates()
            
            self._append('dfsketree',dfsketree)
        except:
            raise Exception('Failed when building skeleton db.')
        
        return 'Sucessfully creating skeleton db.'

    
    def _append(self,dfname,df):
        """Append a dataframe df into database with a name dfname.
        
        Note: 
            If dfname already existed, it will be overwritten.
        
        """
        try:
            store = pd.HDFStore(self.dbname)
            # check existing
            if store.get_storer(dfname) is not None:    
                store.remove(dfname)
            # insert into database
            store.append(dfname,df,format='table',data_columns=True,index=False)
            store.create_table_index(dfname,optlevel=9,kind='full')
            store.close()
        except:
            return 'Failed when appending.'
        return 'Sucessfully appending {} into db.'.format(dfname)
    
    def _root2node(self,nodearr,idx):
        """Extract tree path from root to a given node with index idx.
        """
        subtree = str(nodearr[str(idx)]).split('/')
        subtree[-1] = subtree[-1][0:-2]
        subtree = np.array(subtree[1:]).astype('int')
        return subtree
        
    def _build_skeleton_tree(self,dfske,dfcon):
        """Build tree structure from skeleton coordinates.
        """
        
        # skeleton must have at least 2 points
        ske_count = dfske['skeleton_id'].value_counts()
        skeidarr = ske_count[ske_count >= 2].index.values 
        
        records = []
        
        # category: 0-root, 1-leaf, 2-connector, 3-longest subtree, 4-internode, 5..-branch
        for skeid in skeidarr:
            sub_dfske = dfske[dfske['skeleton_id']==skeid]
            sub_dfske = sub_dfske.set_index(['treenode_id'])
            
            # add root node
            rootnode = int(sub_dfske[sub_dfske['parent_treenode_id']==-1].index[0])
            records.append([skeid,rootnode,0])
            
            # build tree nodes
            nodearr = {}
            for i in range(len(sub_dfske)):
                nodeid = int(sub_dfske.index[i])
                nodearr[str(nodeid)] = anytree.Node(nodeid)
            for i in range(len(sub_dfske)):
                nodeid = int(sub_dfske.index[i])
                parentnodeid = int(sub_dfske.iloc[i]['parent_treenode_id'])
                if parentnodeid != -1:
                    # second, add parent nodes
                    nodearr[str(nodeid)].parent = nodearr[str(parentnodeid)] 
                
            # find leaf nodes
            nodeset = set(sub_dfske.index.values.astype('int'))
            parentset = set(sub_dfske['parent_treenode_id'].values.astype('int'))
            leaves = list(nodeset.difference(parentset))
    
            # add leaf nodes
            for node in leaves:
                records.append([skeid,node,1])
                
            # add connector nodes
            conlst = dfcon[dfcon['skeleton_id']==skeid]['treenode_id'].unique().astype('int') # it may happen similar (ske,treenode) for 2 different connector id
            if len(conlst)>0:
                for node in conlst:
                    records.append([skeid,node,2])
    
            # extract subtree paths
            subtree = []
            subtreelen = []
            for ileaf in leaves:
                leaf = self._root2node(nodearr,ileaf)
                subtree.append(leaf)
                subtreelen.append(len(leaf))
    
            # find longest subtree (i.e. subtree with maximal number of nodes)
            longest_id = np.array(subtreelen).argmax() # the longest subtree
            for node in subtree[longest_id]:
                records.append([skeid,node,3])
    
            # find inter nodes
            parentarr = sub_dfske['parent_treenode_id'].values.astype('int')
            # nb. of times being a parent for a node
            nodes, count = np.unique(parentarr,return_counts=True) 
            idx = np.argwhere(count>=2).flatten()
    
            # if there exist branches
            if len(idx)>0:
                internodes = nodes[idx].tolist() # node has at least two chidren
                # nodes used to examine branches, including leaves, internodes and root (0)
                controlnodes = leaves + internodes + [rootnode] 
                branches = []
    
                # add inter nodes
                for node in internodes:
                    records.append([skeid,node,4])
                
                # extract branches    
                # for each control node, extract the path from root to the control node, 
                # then find the closest control node in the path
                for it in range(len(controlnodes)):
                    branch = self._root2node(nodearr,controlnodes[it])
                    idx = len(branch)-2
                    while(idx != -1):
                        if branch[idx] in controlnodes:
                            subbranch = branch[idx:]
                            branches.append(subbranch)
                            break
                        idx = idx - 1
    
                # add branches
                branchid = 5
                for branch in branches:
                    for node in branch:
                        records.append([skeid,node,branchid])
                    branchid = branchid + 1
        
        return pd.DataFrame(records,columns=['skeleton_id','treenode_id','category'])
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
