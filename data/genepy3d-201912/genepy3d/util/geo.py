# -*- coding: utf-8 -*-

import numpy as np
from scipy.ndimage.measurements import label
from scipy.ndimage.filters import gaussian_filter1d

from sklearn import metrics

class RTable:
    """Compute scales lookup table for osculating radii.
    """
    
    def __init__(self,l=1,rmin=1,rmax=100,smin=0,smax=200,mo="nearest",kerlen=4.0,save_circles=False):
        """
        
        Args:
            l (int): sampled unit length.
            rmin, rmax (int): range of osculating radii.
            smin, smax (int): range of scales to be calculated.
        
        """
        
        tol = 0.
        
        rlst = np.arange(rmin,rmax+1)
        slst = np.arange(smin,smax+1)
        
        # compute sampled points
        nrlst = (np.round((np.pi * rlst)/l)).astype(np.int)
        ix = np.argwhere(nrlst<3).flatten()
        nrlst[ix] = 3
        
        rtab = np.zeros((len(rlst),len(slst)))
        
        if save_circles==True:
            circles = {}
        
        for i in range(len(rlst)):
    
            theta = np.linspace(0,np.pi,nrlst[i])
            x = rlst[i] * np.cos(theta)
            y = rlst[i] * np.sin(theta)
            
            if save_circles==True:
                tmp = {}
                tmp[-1]= np.array([x,y]).T
            
            for j in range(len(slst)):
                if slst[j]==0:
                    xs, ys = x.copy(), y.copy()
                else:
                    xs = gaussian_filter1d(x,slst[j],mode=mo,truncate=kerlen)
                    ys = gaussian_filter1d(y,slst[j],mode=mo,truncate=kerlen)
                    
                ps = np.array([xs,ys]).T
                
                if save_circles==True:
                    tmp[j] = ps
                
                kap = self._kappa(ps)
                
                if kap <= tol:
                    rtab[i,j] = -1
                else:
                    rtab[i,j] = 1 / kap
                
            if save_circles==True:
                circles[i] = tmp

        # assign infinite rtab by the maximum
        ix = np.argwhere(rtab==-1)
        if len(ix)!=0:
            rtab_max = np.max(rtab)
            rtab[ix[:,0],ix[:,1]] = rtab_max
        
        self.rlst = rlst
        self.slst = slst
        self.rtab = rtab
        if save_circles==True:
            self.circles = circles
    
    def _kappa(self,p):
        """Compute 2D curvatures.
        """
        
        def _derv(deg,p,dt=1):
            dx, dy = p[:,0].copy(), p[:,1].copy()
            for i in range(deg):
                dx = np.gradient(dx,dt,edge_order=1)
                dy = np.gradient(dy,dt,edge_order=1)
                
            return np.array([dx,dy]).T
        
        d1 = _derv(1,p)
        d2 = _derv(2,p)
        
        nomi = np.abs(d1[:,0]*d2[:,1] - d1[:,1]*d2[:,0])
        deno = (d1[:,0]**2 + d1[:,1]**2)**(1.5)
        
        res = np.zeros(len(deno))
        idx = np.argwhere(deno!=0).flatten()
        if len(idx)!=0:
            res[idx]  = nomi[idx]/deno[idx]
            
        return np.mean(res)
    
    def get_scale(self,r):
        """Estimate the scale corresponding to given radius.
        """
        
        rmask = (self.rtab >= r)*1. # create mask from lookup table
        ids = np.argwhere(self.rlst<r).flatten() # find radius smaller than the referenced radius
        
        if len(ids)==0: # if all is possible
            sig_c, delta_c = 0, 0
        else:
            selected_sig = []
            for i in ids:
                # check if all equal to 0, exclude it
                if(len(np.argwhere(rmask[i]==0).flatten())<len(rmask[i])):
                    selected_sig.append(np.argmax(rmask[i]))
            if len(selected_sig)!=0:
                sig_c = np.median(selected_sig) # compute sig_c
                delta_c = np.std(selected_sig) # compute delta_c
            else:
                sig_c, delta_c = -1, -1
        
        return (sig_c,delta_c,rmask)
        

def norm(P):
    """Norm of points.
    
    Args
        P (array-like [float]): list of points.
    
    """
    
    if len(P.shape)==2: # list of points
        return np.sqrt(np.sum(P**2,axis=1))
    elif len(P.shape)==1: # only one point
        return np.sqrt(np.sum(P**2))
    else:
        raise ValueError('Accept only vector or array of vectors.')

def angle_threepoint(a,b,c):
    """Calculate angle between three points where b is the center point.
    """
    ab = b - a
    bc = c - b
    cosine_angle = np.dot(ab, bc) / (np.linalg.norm(ab) * np.linalg.norm(bc))
    return np.degrees(np.arccos(cosine_angle))

def geo_len(P):
    """Compute geodesic length from a set of points P in nD (n=1,2).
    """
    n = P.shape[0]
    s = 0
    for i in range(n-1):
        p1, p2 = P[i], P[i+1]
        s = s + np.sqrt(np.sum((p1-p2)**2))
    return s

def points_transform(P,phi,theta,psi,dx,dy,dz,zx=1,zy=1,zz=1):
    """Apply transformation for a set of 3d points.
    
    Args:
        P (3d numpy array (float)): list of 3d points.
        phi (float): rotation in x.
        theta (float): rotation in y.
        psi (float): rotation in z.
        dx, dy, dz (float): translations in x, y and z.
        zx, zy, zz (float): zoom factors
    
    Returns:
        Pt (3d numpy array (float)): transformed points.
    
    """
    Pnew = P.copy()
    Pnew = np.append(Pnew,np.ones((Pnew.shape[0],1)),axis=1) # add additional column for 3d transformation => [n, 4]
    Pnew = Pnew[:,:,np.newaxis] # add additional dimension for propagation of all points => [n, 4, 1]
    
    # translation
    Rt = np.array([
        [1., 0., 0., dx],
        [0., 1., 0., dy],
        [0., 0., 1., dz],
        [0., 0., 0., 1.],
    ])
    
    Pt = np.matmul(Rt,Pnew)
    
    # rotation in z
    Rpsi = np.array([
        [np.cos(psi), -np.sin(psi), 0., 0.],
        [np.sin(psi), np.cos(psi), 0., 0.],
        [0., 0., 1., 0.],
        [0., 0., 0., 1.],
    ])
    
    Pt = np.matmul(Rpsi,Pt)
    
    # rotation in y
    Rtheta = np.array([
        [np.cos(theta), 0, np.sin(theta), 0.],
        [0., 1., 0., 0.],
        [-np.sin(theta), 0., np.cos(theta), 0.],
        [0., 0., 0., 1.],
    ])
    
    Pt = np.matmul(Rtheta,Pt)
    
    # rotation in x
    Rphi = np.array([
        [1., 0., 0., 0.],
        [0., np.cos(phi), -np.sin(phi), 0.],
        [0., np.sin(phi), np.cos(phi), 0.],
        [0., 0., 0., 1.],
    ])
    
    Pt = np.matmul(Rphi,Pt)
    
    # zoom
    Rz = np.array([
        [zx, 0., 0., 0.],
        [0., zy, 0., 0.],
        [0., 0., zz, 0.],
        [0., 0., 0., 1.],
    ])
    
    Pt = np.matmul(Rz,Pt)
    
    return Pt[:,:3,0]

def active_brownian_2d(n,v=1e-6,omega=0.,p0=[0,0],dt=1e-3,R=1e-6,T=300.,eta=1e-3,seed_point=None):
    """Generate an active Brownian motion in 2D.
    
    Args:
        n (int): number of times of motion.
        p0 (1d array like): init position.
        v (float): translation speed.
        omega (float): rotation speed.
        dt (float): time step.
        R (float): particle radius.
        T (float): environment temperature.
        eta (float): fluid viscocity.
    
    Returns:
        P (2d numpy array): list of positions.
        t (1d array like): corresponding times.
        
    """
    if seed_point is not None:
        np.random.seed(seed_point)
    
    kB = 1.38e-23 # Boltzmann constant [J/K]
    gamma = 6*np.pi*R*eta # friction coefficient [Ns/m]
    DT = (kB*T)/gamma # translational diffusion coefficient [m^2/s]
    DR = (6*DT)/(8*R**2) # rotational diffusion coefficient [rad^2/s]
    
    P = np.zeros((n,2))
    P[0] = p0 # init point
    
    theta = 0 # init angle
    
    for i in range(n-1):
        # translational diffusion step
        P[i+1] = P[i] + np.sqrt(2*DT*dt)*np.random.randn(1,2)
    
        # rotational diffusion step
        theta = theta + np.sqrt(2*DR*dt)*np.random.randn(1,1)[0,0]
        
        # torque step
        theta = theta + dt*omega

        # drift step
        P[i+1] = P[i+1] + dt*v*np.array([np.cos(theta), np.sin(theta)])
    
    t = np.arange(0,n*dt,dt)
    
    return (P,t)

def active_brownian_3d(n,v=1e-6,omega=0.,p0=[0,0,0],dt=1e-3,R=1e-6,T=300.,eta=1e-3,seed_point=None):
    """Generate an active Brownian motion in 3D.
    
    Args:
        n (int): number of times of motion.
        p0 (1d array like): init position.
        v (float): translation speed.
        omega (float): rotation speed.
        dt (float): time step.
        R (float): particle radius.
        T (float): environment temperature.
        eta (float): fluid viscocity.
    
    Returns:
        P (2d numpy array): list of positions.
        t (1d array like): corresponding times.
        
    """
    
    if seed_point is not None:
        np.random.seed(seed_point)
    
    kB = 1.38e-23 # Boltzmann constant [J/K]
    DT = (kB*T)/(6*np.pi*eta*R) # translational diffusion coefficient [m^2/s]
    DR = (kB*T)/(8*np.pi*eta*R**3) # rotational diffusion coefficient [rad^2/s]
    
    P = np.zeros((n,3))
    P[0] = p0 # init point
    
    theta = 0 # init angle
    phi = 0 # init angle
    
    for i in range(n-1):
        # translational diffusion step
        P[i+1] = P[i] + np.sqrt(2*DT*dt)*np.random.randn(1,3)
    
        # rotational diffusion step
        theta = theta + np.sqrt(2*DR*dt)*np.random.randn(1,1)[0,0]
        phi = phi + np.sqrt(2*DR*dt)*np.random.randn(1,1)[0,0]
        
        # torque step
        if omega != 0:
            theta = theta + dt*np.sin(omega)
            phi = phi + dt*np.cos(omega)

        # drift step
        P[i+1] = P[i+1] + dt*v*np.array([np.cos(theta)*np.sin(phi), np.sin(theta)*np.sin(phi), np.cos(phi)])
    
    t = np.arange(0,n*dt,dt)
    
    return (P,t)

def fit_plane(P):
    """Fitting plane from set of points P.
    """
    (rows, cols) = P.shape
    G = np.ones((rows, 3))
    G[:, 0] = P[:, 0]  #X
    G[:, 1] = P[:, 1]  #Y
    Z = P[:, 2]
    (a, b, c),resid,rank,s = np.linalg.lstsq(G, Z)
    normal = (a, b, -1)
    nn = np.linalg.norm(normal)
    normal = normal / nn
    return (c, normal)

def decompose_intrinsicdim_sequential(p,eps_line,eps_plane,eps_seg):
    """Decompose sequentially intrinsic segments with priority line to plane to 3d
    
    from paper of Jianyu Yang: https://www.sciencedirect.com/science/article/pii/S1047320316300438
    
    """
    
    # support functions
    # ---
    
    def determinant(a,b,c,d):
        
        ab = b - a
        ac = c - a
        ad = d - a        
        m = np.array([[ab[0], ab[1], ab[2]],
                      [ac[0], ac[1], ac[2]],
                      [ad[0], ad[1], ad[2]]])
    
        return abs((1./6)*np.linalg.det(m))
    
    def label_line(p,eps,seg_len):
        n = len(p)
        lbl = np.zeros(n,dtype=np.uint)
        for i in range(1,n-1):
            p1 = p[i]-p[i-1]
            p2 = p[i+1]-p[i]
            
            if((norm(p1)==0) | (norm(p2)==0)):
                lbl[i]=0 # collapse points
            else:
                delta = 1. - ((np.dot(p1,p2)) / (norm(p1)*norm(p2)))
                if delta <= eps:
                    lbl[i] = 1 # line
                else:
                    lbl[i] = 0
                
        # remove small segment
        connected_compo, nb_compo = label(lbl)
        if nb_compo!=0:
            for j in range(1,nb_compo+1):
                connected_idx = np.argwhere(connected_compo==j).flatten()
                if len(connected_idx)<=seg_len:
                    lbl[connected_idx] = 0        
        
        return lbl
        
    def label_plane(p,lbl,eps,seg_len):
        n = len(p)
        lbl2 = lbl.copy()
        for i in range(1,n-2):
            if((lbl2[i]==0) & (lbl2[i+1]==0)):
                delta = determinant(p[i-1],p[i],p[i+1],p[i+2])                    
                if delta <= eps:
                    lbl2[i] = 2 # plane
                    lbl2[i+1] = 2
                    
        # remove small segment
        idx = np.argwhere(lbl2==2).flatten()
        lbltmp = np.zeros(n,dtype=np.uint)
        lbltmp[idx]=1
        connected_compo, nb_compo = label(lbltmp)
        if nb_compo!=0:
            for j in range(1,nb_compo+1):
                connected_idx = np.argwhere(connected_compo==j).flatten()
                if len(connected_idx)<=seg_len:
                    lbl2[connected_idx] = 0
        
        return lbl2
    
    def get_segments(lbl):
    
        data = []
        for ilbl in [0,1,2]:
            segs = []
            indicator = np.zeros(len(lbl))
            tmp = np.argwhere(lbl==ilbl).flatten()
            indicator[tmp]=1
            connected_compo, nb_compo = label(indicator)
            if nb_compo!=0:
                for j in range(1,nb_compo+1):
                    idx = list(np.argwhere(connected_compo==j).flatten())
                    if len(idx)!=0:
                        segs.append([idx[0],idx[-1]])
            
            data.append(segs)
        
        return data
    
    # ---
    
    lbl1 = label_line(p,eps_line,eps_seg)
    lbl2 = label_plane(p,lbl1,eps_plane,eps_seg)
    segs = get_segments(lbl2)
    
    return (segs,lbl2)

#def decompose_intrinsicdim_sequential(p,eps_line,eps_plane,eps_seg):
#    """Decompose sequentially intrinsic segments with priority line to plane to 3d
#    
#    from paper of Jianyu Yang: https://www.sciencedirect.com/science/article/pii/S1047320316300438
#    
#    """
#    
#    # support functions
#    # ---
#    
#    def determinant(a,b,c,d):
#        
#        ab = b - a
#        ac = c - a
#        ad = d - a        
#        m = np.array([[ab[0], ab[1], ab[2]],
#                      [ac[0], ac[1], ac[2]],
#                      [ad[0], ad[1], ad[2]]])
#    
#        return abs((1./6)*np.linalg.det(m))
#    
#    def label_line(p,eps,seg_len):
#        n = len(p)
#        lbl = np.zeros(n,dtype=np.uint)
#        for i in range(1,n-1):
#            p1 = p[i]-p[i-1]
#            p2 = p[i+1]-p[i]
#            
#            if((norm(p1)==0) | (norm(p2)==0)):
#                lbl[i]=0 # collapse points
#            else:
#                delta = 1. - ((np.dot(p1,p2)) / (norm(p1)*norm(p2)))
#                if delta <= eps:
#                    lbl[i] = 1 # line
#                else:
#                    lbl[i] = 0
#                
#        # remove small segment
#        connected_compo, nb_compo = label(lbl)
#        if nb_compo!=0:
#            for j in range(1,nb_compo+1):
#                connected_idx = np.argwhere(connected_compo==j).flatten()
#                if len(connected_idx)<=seg_len:
#                    lbl[connected_idx] = 0        
#        
#        return lbl
#    
#    def label_other(p,lbl,eps):
#        n = len(p)
#        lbl2 = lbl.copy()
#        for i in range(1,n-2):
#            if((lbl2[i]==0) & (lbl2[i+1]==0)):
#                delta = determinant(p[i-1],p[i],p[i+1],p[i+2])                    
#                if delta <= eps:
#                    lbl2[i] = 2 # plane
#                    lbl2[i+1] = 2
#                else:
#                    lbl2[i] = 3 # 3d
#                    lbl2[i+1] = 3
#                    
#        return lbl2
#    
#    def get_segments(lbl):
#    
#        data = []
#        for ilbl in [0,1,2,3]:
#            segs = []
#            indicator = np.zeros(len(lbl))
#            tmp = np.argwhere(lbl==ilbl).flatten()
#            indicator[tmp]=1
#            connected_compo, nb_compo = label(indicator)
#            if nb_compo!=0:
#                for j in range(1,nb_compo+1):
#                    idx = list(np.argwhere(connected_compo==j).flatten())
#                    if len(idx)!=0:
#                        segs.append([idx[0],idx[-1]])
#            
#            data.append(segs)
#        
#        return data
#    
#    # ---
#    
#    lbl1 = label_line(p,eps_line,eps_seg)
#    lbl2 = label_other(p,lbl1,eps_plane)
#    segs = get_segments(lbl2)
#    
#    return (segs,lbl2)

def eval_score(true_line_ids,pred_line_ids,true_plane_ids,pred_plane_ids,n,acc_metric="f1"):
    """Metric to evaluate plane and non-plane accuracies
    
    Args:
        true_plane_ids: list of true planes indices, [[0,100],[101,150],[300,400], ...]
        pred_plane_ids: list of predicted planes indices
        true_line_ids: list of true lines indices, [[0,100],[101,150],[300,400], ...]
        pred_line_ids: list of predicted lines indices
        n: total number of points
    
    """
    
    def _get_segs(P):
        """Get list of local planes or lines for a given scale of the planes or lines evolution data.
        """
        seg_list = []
        connected_compo, nb_compo = label(P)
        if nb_compo != 0:
            for i_compo in range(1,nb_compo+1):
                idx = np.argwhere(connected_compo==i_compo).flatten()
                seg_list.append([idx[0],idx[-1]])
        return seg_list
    
    # line score
    if((len(true_line_ids)==0) | (len(pred_line_ids)==0)):
        line_acc = -1 # not consider this case for simplcity
        line_pre = -1
        line_rec = -1
    else:
        acc_line = []
        pre_line, rec_line = [], []
        for true_idx in true_line_ids:
            true_tmp = np.zeros(n,dtype=np.uint)
            true_tmp[true_idx[0]:true_idx[-1]+1]=1
            acc = 0.
            pre, rec = 0., 0.
            for pred_idx in pred_line_ids:
                pred_tmp = np.zeros(n,dtype=np.uint)
                pred_tmp[pred_idx[0]:pred_idx[-1]+1]=1
                
                pre_tmp = metrics.precision_score(true_tmp,pred_tmp)
                if pre_tmp > pre:
                    pre = pre_tmp
                
                rec_tmp = metrics.recall_score(true_tmp,pred_tmp)
                if rec_tmp > rec:
                    rec = rec_tmp
                
                if acc_metric=="f1":
                    acc_tmp = metrics.f1_score(true_tmp,pred_tmp)
                elif acc_metric=="jaccard":
                    acc_tmp = metrics.jaccard_score(true_tmp,pred_tmp)
                else:
                    acc_tmp = metrics.balanced_accuracy_score(true_tmp,pred_tmp)
                
                if acc_tmp > acc:
                    acc = acc_tmp
            
            acc_line.append(acc)
            pre_line.append(pre)
            rec_line.append(rec)
        
        line_acc = np.mean(np.array(acc_line))
        line_pre = np.mean(np.array(pre_line))
        line_rec = np.mean(np.array(rec_line))
    
    # plane score
    if((len(true_plane_ids)==0) | (len(pred_plane_ids)==0)):
        plane_acc = -1 # not consider this case for simplcity
        plane_pre = -1
        plane_rec = -1
    else:
        acc_plane = []
        pre_plane, rec_plane = [], []
        for true_idx in true_plane_ids:
            true_tmp = np.zeros(n,dtype=np.uint)
            true_tmp[true_idx[0]:true_idx[-1]+1]=1
            acc = 0.
            pre, rec = 0., 0.
            for pred_idx in pred_plane_ids:
                pred_tmp = np.zeros(n,dtype=np.uint)
                pred_tmp[pred_idx[0]:pred_idx[-1]+1]=1
                
                pre_tmp = metrics.precision_score(true_tmp,pred_tmp)
                if pre_tmp > pre:
                    pre = pre_tmp
                
                rec_tmp = metrics.recall_score(true_tmp,pred_tmp)
                if rec_tmp > rec:
                    rec = rec_tmp                
                
                if acc_metric=="f1":
                    acc_tmp = metrics.f1_score(true_tmp,pred_tmp)
                elif acc_metric=="jaccard":
                    acc_tmp = metrics.jaccard_score(true_tmp,pred_tmp)
                else:
                    acc_tmp = metrics.balanced_accuracy_score(true_tmp,pred_tmp)
                
                if acc_tmp > acc:
                    acc = acc_tmp
                        
            acc_plane.append(acc)
            pre_plane.append(pre)
            rec_plane.append(rec)
        
        plane_acc = np.mean(np.array(acc_plane))
        plane_pre = np.mean(np.array(pre_plane))
        plane_rec = np.mean(np.array(rec_plane))
    
    # 3d score
    tmp = np.ones(n,dtype=np.uint)
    for ip in (true_plane_ids+true_line_ids):
        tmp[ip[0]:ip[-1]+1]=0
    true_nonplane_ids = _get_segs(tmp)
    # print(true_nonplane_ids)
    
    tmp = np.ones(n,dtype=np.uint)
    for ip in (pred_plane_ids+pred_line_ids):
        tmp[ip[0]:ip[-1]+1]=0
    pred_nonplane_ids = _get_segs(tmp)
    # print(pred_nonplane_ids)
    
    if((len(true_nonplane_ids)==0) | (len(pred_nonplane_ids)==0)):
        nonplane_acc = -1 # not consider this case for simplcity
        nonplane_pre = -1
        nonplane_rec = -1
    else:
        acc_nonplane = []
        pre_nonplane, rec_nonplane = [], []
        for true_idx in true_nonplane_ids:
            true_tmp = np.zeros(n,dtype=np.uint)
            true_tmp[true_idx[0]:true_idx[-1]+1]=1
            acc = 0.
            pre, rec = 0., 0.
            for pred_idx in pred_nonplane_ids:
                pred_tmp = np.zeros(n,dtype=np.uint)
                pred_tmp[pred_idx[0]:pred_idx[-1]+1]=1
                
                pre_tmp = metrics.precision_score(true_tmp,pred_tmp)
                if pre_tmp > pre:
                    pre = pre_tmp
                
                rec_tmp = metrics.recall_score(true_tmp,pred_tmp)
                if rec_tmp > rec:
                    rec = rec_tmp
                
                if acc_metric=="f1":
                    acc_tmp = metrics.f1_score(true_tmp,pred_tmp)
                elif acc_metric=="jaccard":
                    acc_tmp = metrics.jaccard_score(true_tmp,pred_tmp)
                else:
                    acc_tmp = metrics.balanced_accuracy_score(true_tmp,pred_tmp)
                
                if acc_tmp > acc:
                    acc = acc_tmp
                        
            acc_nonplane.append(acc)
            pre_nonplane.append(pre)
            rec_nonplane.append(rec)
        
        nonplane_acc = np.mean(np.array(acc_nonplane))
        nonplane_pre = np.mean(np.array(pre_nonplane))
        nonplane_rec = np.mean(np.array(rec_nonplane))

    return ([line_acc, line_pre, line_rec],
            [plane_acc, plane_pre, plane_rec],
            [nonplane_acc, nonplane_pre, nonplane_rec])
#    return (line_score,plane_score,nonplane_score)
    
def eval_score2(true_line_ids,pred_line_ids,true_plane_ids,pred_plane_ids,n,acc_metric="f1"):
    """Metric to evaluate plane and non-plane accuracies
    
    Args:
        true_line_ids: list of true lines indices, [[0,100],[101,150],[300,400], ...]
        pred_line_ids: list of predicted lines indices
        true_plane_ids: list of true planes indices, [[0,100],[101,150],[300,400], ...]
        pred_plane_ids: list of predicted planes indices
        n: total number of points
        acc_metric: support "f1"|"jaccard"|"unbalanced"
    
    """
    
    def _eval(true_ids,pred_ids,acc_metric="f1"):
        precision = metrics.precision_score(true_ids,pred_ids)
        recall = metrics.recall_score(true_ids,pred_ids)
        if acc_metric=="f1":
            acc = metrics.f1_score(true_ids,pred_ids)
        elif acc_metric=="jaccard":
            acc = metrics.jaccard_score(true_ids,pred_ids)
        else:
            acc = metrics.balanced_accuracy_score(true_ids,pred_ids)
        return (acc,precision,recall)
        
    # line score
    if((len(true_line_ids)==0) | (len(pred_line_ids)==0)):
        line_acc = -1 # not consider this case for simplcity
        line_pre = -1
        line_rec = -1
    else:
        true_line_flag = np.zeros(n,dtype=np.uint)
        for ip in (true_line_ids):
            true_line_flag[ip[0]:ip[-1]+1]=1
        
        pred_line_flag = np.zeros(n,dtype=np.uint)
        for ip in pred_line_ids:
            pred_line_flag[ip[0]:ip[-1]+1]=1
            
        line_acc, line_pre, line_rec = _eval(true_line_flag,pred_line_flag,acc_metric)
    
    # plane score
    if((len(true_plane_ids)==0) | (len(pred_plane_ids)==0)):
        plane_acc = -1 # not consider this case for simplcity
        plane_pre = -1
        plane_rec = -1
    else:
        true_plane_flag = np.zeros(n,dtype=np.uint)
        for ip in (true_plane_ids):
            true_plane_flag[ip[0]:ip[-1]+1]=1
        
        pred_plane_flag = np.zeros(n,dtype=np.uint)
        for ip in pred_plane_ids:
            pred_plane_flag[ip[0]:ip[-1]+1]=1
            
        plane_acc, plane_pre, plane_rec = _eval(true_plane_flag,pred_plane_flag,acc_metric)
    
    # 3d score
    true_threed_flag = np.ones(n,dtype=np.uint)
    for ip in (true_plane_ids+true_line_ids):
        true_threed_flag[ip[0]:ip[-1]+1]=0
        
    pred_threed_flag = np.ones(n,dtype=np.uint)
    for ip in (pred_plane_ids+pred_line_ids):
        pred_threed_flag[ip[0]:ip[-1]+1]=0
    
    threed_acc, threed_pre, threed_rec = _eval(true_threed_flag,pred_threed_flag,acc_metric)
    
    return ([line_acc, line_pre, line_rec],
            [plane_acc, plane_pre, plane_rec],
            [threed_acc, threed_pre, threed_rec])

































