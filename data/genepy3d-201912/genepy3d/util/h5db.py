"""
Saving complexe structures such as tree, surface into table style database like h5db might
be not useful. 

This module is temporary skipped.

"""

import pandas as pd

class H5Db:
    """Support read/write to Hdf5 format.
    
    Attributes:
        dbname (str): hdf5 file path.
    
    """
    def __init__(self,dbname):
        """Set Hdf5 file path.
        
        Args:
            dbname (str): hdf5 file path.
        
        """
        self.dbname = dbname
        
    def append(self,dfname,df):
        """Append a dataframe df into database with a name dfname.
        
        Note: 
            If dfname already existed, it will be overwritten.
        
        """
        
        if len(df)==0:
            return 'The dataframe {} must have at least one row.'.format(dfname)
        
        # open for appending
        store = pd.HDFStore(self.dbname)
        
        # check if the dfname existing in the db
        # NOTE: this only work if dfname was stored at the first level "/".
        if "/"+dfname in store.keys():
            store.remove(dfname)
            store.flush()
            
        try:
            # insert into database
            store.append(dfname,df,format='table',data_columns=True,index=False)
            store.create_table_index(dfname,optlevel=9,kind='full')
            store.flush()
            store.close()
        except:
            raise Exception('Failed when appending '+dfname)
        
        return 'Sucessfully appending {} into db.'.format(dfname)
        
    def get_dataframe(self,name,columns=None,query=None):
        """Return a dataframe, reuse select() method in HDFStore class of Pandas library.
        
        Args:
            name (str): table name.
            columns (list [str]): list of columns names. If None, then all columns are loaded.
            query (str): query string.
            
        Returns:
            Pandas dataFrame.
        
        """
        store = pd.HDFStore(self.dbname,mode='r')
        df = store.select(name,where=query,columns=columns)   
        store.close()
        return df
    
    
    
    
    
    
    
    
    
    
    
    
    
    
