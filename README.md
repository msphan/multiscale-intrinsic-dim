# Multiscale intrinsic dimension decomposition of 3D neuronal trace

We propose a new analysis of neuronal arbors for better understanding of their spatial complexity. 
The neuronal trace in 3D is decomposed into sequences of local sections classifying as 1D, 2D or 3D.
A local 3d scale is calculated from such decomposition and can be used to study the spatial local structures of the neuronal trace.
The methods is implemented in GeNePy3D (https://genepy3d.gitlab.io), an open source python library for quantitative geometry in neuroscience, 
providing a complete manipulation of various kinds of geometrical objects such as trees, curves, point cloud, surfaces.

This repo contains easy-to-use python notebooks and published datasets allowing you to try our proposed measurement in different contexts.
In order to run the notebooks, you need python > 3.6, the GeNePy3D library and the jupyter notebook.

## Prerequirements

### Python

If you haven't installed python, we recommend to use Anaconda, an open-source distribution including all things necessary for working with python. 
A mini version of Anaconda, Miniconda is enough to run the code.

- Anaconda download page: https://www.anaconda.com/products/individual
- Miniconda download page: https://docs.conda.io/en/latest/miniconda.html

### Dependent packages

`pip install genepy3d statsmodels notebook`

## Exploration step by step

We provide many notebooks for many case studies of local 3D scales. They are mostly separated into *process* and *analyse* parts. The process step is sometime long (30 minutes to several hours). We already saved the processed results and you can quickly exploit them with *analyse* notebooks.

### Validation

We first suggest you to try out the notebooks in *validation* folder. 

1. *local_3d_scale_cell_types* notebooks examine the local 3D scales in various cell types
2. if you are interested in the underlined algorithm, please check 
 - *dim_decomposition_eval* notebooks for intrinsic dimensional decomposition, a fundamental algorithm used to compute the local 3D scales.
 - *local_3d_scale_scheme_illustration* notebook as an example pipeline for computation of local 3D scales.

### DSCAM

We studied local 3D scales of neuronal arbors during their growth. We reanalysed the dataset of Santos et al., 2018 containing dendritic arbors of Xenopus laevis tectal neurons during development under the effect of down syndrome cell adhesion molecule (DSCAM). Please check the notebooks in the *dscam* folder. 

### Larval zebrafish brain

The second dataset is the whole brain Larval zebrafish from Kunst et al., 2019. We studied local 3D scales across 36 brain regions, by various cell types and also go deeper into a specific brain region (Torus Semicircularis). The notebooks are in the *zebrafish* folder.

1. *zebrafish_brain_regions* depicts how local 3D scales distribute across inter-regions.
2. *zebrafish_midline* checks the local 3D scales at the midline.
3. *zebrafish_cell_types* checks the local 3D scales in different cell types.
4. *zebrafish_torus* advanced studies of local 3D scales for Torus Semicircularis.




