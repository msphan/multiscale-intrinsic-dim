{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Processing steps for computing local 3D scale of neurons in developing Xenopus laevis tadpoles under effect of down syndrome cell adhesion molecule (DSCAM).\n",
    "\n",
    "**Note:** run this section can take pretty much time (~40 minutes). The results were saved in \"../data/dscam/result/\". You can check \"dscam_analyse.ipynb\" if you want to quickly exploit the results."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Packages importation and general configuration"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import seaborn as sns\n",
    "\n",
    "from functools import partial\n",
    "import pathos.pools as pp\n",
    "\n",
    "import scipy.stats as stats\n",
    "from scipy.stats import ttest_ind\n",
    "import statsmodels.api as sm\n",
    "from statsmodels.formula.api import ols\n",
    "from statsmodels.sandbox.stats.multicomp import multipletests\n",
    "\n",
    "import matplotlib as mpl\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib import cm\n",
    "from matplotlib.colors import ListedColormap\n",
    "from mpl_toolkits import mplot3d"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "from genepy3d.io import swc\n",
    "from genepy3d.obj import curves, trees, points\n",
    "from genepy3d.util import geo, plot as mypl"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib notebook"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Reading neuronal traces from SWC files"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# initialize swc importer\n",
    "filepath = \"../data/dscam/swc/\"\n",
    "f = swc.SWC(filepath,recursive=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "nb. of imported neurons: 412\n"
     ]
    }
   ],
   "source": [
    "print(\"nb. of imported neurons:\",len(f.get_neuron_id()))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Setting scale range\n",
    "\n",
    "The maximal scale (rmax) is defined based on the length of the longest branch in dataset (i.e. rmax = 1/3 * 180 microns)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "list of scales: [ 1  3  5  7  9 11 13 15 17 19 21 23 25 27 29 31 33 35 37 39 41 43 45 47\n",
      " 49 51 53 55 57 59]\n"
     ]
    }
   ],
   "source": [
    "rmin, rmax = 1, 61\n",
    "r_lst = np.arange(rmin,rmax,2)\n",
    "print(\"list of scales:\",r_lst)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Computing local 3D scales"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Unity function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "def unity(neuid):\n",
    "    \"\"\"Compute neuronal trace features such as local 3D scale, length, tortuosity.\n",
    "    \n",
    "    Args:\n",
    "        neuid (int): neuron ID\n",
    "        \n",
    "    Returns:\n",
    "        features such as number of branchings, arbor length, tortuosity, local 3D scale.\n",
    "    \n",
    "    \"\"\"\n",
    "    \n",
    "    # parameters setting\n",
    "    sample_length = 1 # unit sample length (in micron)\n",
    "    lmin = 5 # smallest length segment to be analysed (in micron), half of minimal length of brain region\n",
    "\n",
    "    dim_param = {\n",
    "        \"lmin\":lmin,\n",
    "        \"sig_step\":1,\n",
    "        \"eps_kappa\":0.01,\n",
    "        \"eps_tau\":0.01,\n",
    "        \"eps_crv_len\":lmin,\n",
    "        \"eps_seg_len\":lmin\n",
    "    }\n",
    "    \n",
    "    rmin, rmax = 1, 61\n",
    "    r_lst = np.arange(rmin,rmax,2)\n",
    "    \n",
    "    # get neuron\n",
    "    neuron = f.get_neurons(neuid)\n",
    "\n",
    "    # check if neuron has many roots, then get tree with maximal number of nodes\n",
    "    rootidlst = neuron.get_root()\n",
    "    rootid = rootidlst[np.argmax([neuron.get_nodes_number(_rootid) for _rootid in rootidlst])]\n",
    "    \n",
    "    # pruning small leaf branching\n",
    "    neuron_pruned = neuron.prune_leaves(length=lmin,rootid=rootid)\n",
    "\n",
    "    # resample\n",
    "    try:\n",
    "        newneuron = neuron_pruned.resample(unit_length=sample_length,rootid=rootid,spline_order=2,decompose_method=\"spine\")\n",
    "    except:\n",
    "        print(\"resampling failed, neuid={}\".format(neuid))\n",
    "    \n",
    "    # local 3D scale\n",
    "    df = newneuron.compute_local_3d_scale(r_lst,dim_param,\"leaf\",rootid)\n",
    "    \n",
    "    # mean local 3d scale\n",
    "    ls_mean = df[\"local_scale\"].mean()\n",
    "    \n",
    "    # number of branchings\n",
    "    nb_branching = len(newneuron.get_branchingnodes())\n",
    "\n",
    "    # total arbor length\n",
    "    arbor_length = newneuron.compute_length()\n",
    "    \n",
    "    # longest branch\n",
    "    spine_nodes = newneuron.compute_spine()\n",
    "\n",
    "    # tortuosity of longest branch\n",
    "    spine_wiggliness = curves.Curve(newneuron.get_coordinates(spine_nodes).values).compute_wiggliness()\n",
    "\n",
    "    # mean 3d scale of longest branch\n",
    "    spine_nodes_valid = np.intersect1d(df.index.values,spine_nodes)\n",
    "    spine_ls_mean = df.loc[spine_nodes_valid][\"local_scale\"].mean()\n",
    "    \n",
    "    df.to_csv(\"../data/dscam/result/neuron_{}.csv\".format(neuid))\n",
    "    np.save(\"../data/dscam/result/neuron_{}.npy\".format(neuid),[nb_branching,arbor_length,ls_mean,spine_ls_mean,spine_wiggliness,newneuron])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running..."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "# test unity function before running in parallel mode\n",
    "unity(neuidlst[0])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# parallel computing setting\n",
    "maxcpu = 35 # maximal number of workers can be used to run the function \n",
    "pool = pp.ProcessPool(min(len(neuidlst),maxcpu))\n",
    "print(\"nb. of used cpus:\",pool.ncpus)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "result = pool.map(unity,neuidlst)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.7"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "922px",
    "left": "33px",
    "top": "111.133px",
    "width": "166px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
